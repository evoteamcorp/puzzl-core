"use strict";
require("reflect-metadata");
var extend = function (target) {
    if (!Reflect.getMetadata("puzzl:extendedConstructor", target)) {
        var f_1 = function () {
            var args = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                args[_i - 0] = arguments[_i];
            }
            return construct(target, args);
        };
        f_1.prototype = target.prototype;
        Reflect.defineMetadata("puzzl:metaName", target.name, f_1);
        function construct(constructor, args) {
            var c = function () {
                var _this = this;
                var overloadsArgs = Reflect.getMetadata("puzzl:overload:constructor:args", f_1);
                if (overloadsArgs) {
                    overloadsArgs.forEach(function (overload) {
                        args = overload(args);
                    });
                }
                var currentObjectModifiers = Reflect.getMetadata("puzzl:overload:constructor:modifier", f_1);
                if (currentObjectModifiers) {
                    currentObjectModifiers.forEach(function (modifier) {
                        args = modifier(_this, args);
                    });
                }
                var beforeApplyRoutines = Reflect.getMetadata("puzzl:overload:constructor:beforeApplyRoutine", f_1);
                if (!beforeApplyRoutines) {
                    beforeApplyRoutines = [];
                }
                var oldP = this.promise;
                this.promise = Promise.each(beforeApplyRoutines, function (routine) {
                    return routine(_this);
                }).then(function () {
                    return new Promise(function (resolve, reject) {
                        constructor.apply(_this, args);
                        if (global._.isFunction(_this.then)) {
                            _this.then(resolve).catch(reject);
                        }
                        else if (oldP && global._.isFunction(oldP.then)) {
                            oldP.then(resolve).catch(reject);
                        }
                        else {
                            resolve();
                        }
                    });
                }).then(function () {
                    return new Promise(function (resolve, reject) {
                        var afterApplyRoutines = Reflect.getMetadata("puzzl:overload:constructor:afterApplyRoutine", f_1);
                        if (afterApplyRoutines) {
                            Promise.each(afterApplyRoutines, function (routine) {
                                return routine(_this);
                            }).then(resolve).catch(reject);
                        }
                        else {
                            resolve();
                        }
                    });
                });
                return this;
            };
            c.prototype = constructor.prototype;
            return new c();
        }
        Reflect.defineMetadata("puzzl:extendedConstructor", true, f_1);
        Reflect.getMetadataKeys(target).forEach(function (key) {
            Reflect.defineMetadata(key, Reflect.getMetadata(key, target), f_1);
        });
        return f_1;
    }
    return target;
};
exports.extend = extend;
var registerOverloadArgs = function (target, overload) {
    var overloads = [];
    if (Reflect.getMetadata("puzzl:overload:constructor:args", target)) {
        overloads = Reflect.getMetadata("puzzl:overload:constructor:args", target);
    }
    overloads.push(overload);
    Reflect.defineMetadata("puzzl:overload:constructor:args", overloads, target);
    return target;
};
exports.registerOverloadArgs = registerOverloadArgs;
var registerInstanceModifier = function (target, modifier) {
    var modifiers = [];
    if (Reflect.getMetadata("puzzl:overload:constructor:modifier", target)) {
        modifiers = Reflect.getMetadata("puzzl:overload:constructor:modifier", target);
    }
    modifiers.push(modifier);
    Reflect.defineMetadata("puzzl:overload:constructor:modifier", modifiers, target);
    return target;
};
exports.registerInstanceModifier = registerInstanceModifier;
var registerBeforeApplyRoutine = function (target, routine) {
    var routines = [];
    if (Reflect.getMetadata("puzzl:overload:constructor:beforeApplyRoutine", target)) {
        routines = Reflect.getMetadata("puzzl:overload:constructor:beforeApplyRoutine", target);
    }
    routines.push(routine);
    Reflect.defineMetadata("puzzl:overload:constructor:beforeApplyRoutine", routines, target);
    return target;
};
exports.registerBeforeApplyRoutine = registerBeforeApplyRoutine;
var registerAfterApplyRoutine = function (target, routine) {
    var routines = [];
    if (Reflect.getMetadata("puzzl:overload:constructor:afterApplyRoutine", target)) {
        routines = Reflect.getMetadata("puzzl:overload:constructor:afterApplyRoutine", target);
    }
    routines.push(routine);
    Reflect.defineMetadata("puzzl:overload:constructor:afterApplyRoutine", routines, target);
    return target;
};
exports.registerAfterApplyRoutine = registerAfterApplyRoutine;
//# sourceMappingURL=ExtendedConstructor.js.map
"use strict";
var ExtendedConstructor_1 = require("./ExtendedConstructor");
require("reflect-metadata");
function MetaObject(definition) {
    return function metaObjectFactory(target, decoratedPropertyName) {
        target = ExtendedConstructor_1.extend(target);
        var defaultOpts = {
            functions: [],
            name: target.name || Reflect.getMetadata("puzzl:metaName", target),
            properties: []
        };
        var lodash = global._ || require("lodash");
        definition = lodash.merge(definition, defaultOpts) || defaultOpts;
        Object.keys(target.prototype).forEach(function (prop) {
            var propDesc = Object.getOwnPropertyDescriptor(target.prototype, prop);
            if (typeof propDesc.value === "function" && !/^_/.test(prop)) {
                definition.functions.push(prop);
            }
            else if (!/^_/.test(prop)) {
                definition.properties.push(prop);
            }
        });
        Reflect.defineMetadata("puzzl:metaObject", definition, target);
        return target;
    };
}
exports.MetaObject = MetaObject;
//# sourceMappingURL=MetaObject.js.map
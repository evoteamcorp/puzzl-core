///<reference path="../../manual_typings/global.d.ts" />
import {InstantiateOptions} from "../../manual_typings/built_in";
import {extend} from "./ExtendedConstructor";
import "reflect-metadata";

export function Instantiate(options?: InstantiateOptions): any {
    let lodash = global._ || require("lodash");
    options = lodash.merge({sharedInstance: true}, options) || {sharedInstance: true};

    return function instanciateFactory(target: Function, decoratedPropertyName?: string): any {
        target = extend(target);
        Reflect.defineMetadata("puzzl:instantiate", true, target);
        Reflect.defineMetadata("puzzl:instantiate:options", options, target);
        return target;
    };
}

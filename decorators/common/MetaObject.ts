import {MetaObjectDefinition} from "../../manual_typings/built_in";
import {extend} from "./ExtendedConstructor";
import "reflect-metadata";

export function MetaObject(definition?: MetaObjectDefinition): any {
    return function metaObjectFactory(target: Function, decoratedPropertyName?: string): any {
        target = extend(target);
        let defaultOpts: MetaObjectDefinition = {
            functions : [],
            name      : target.name || Reflect.getMetadata("puzzl:metaName", target),
            properties: []
        };
        let lodash = global._ || require("lodash");
        definition = lodash.merge(definition, defaultOpts) || defaultOpts;
        Object.keys(target.prototype).forEach(
            (prop: string): void => {
                let propDesc = Object.getOwnPropertyDescriptor(target.prototype, prop);
                if (typeof propDesc.value === "function" && !/^_/.test(prop)) {
                    definition.functions.push(prop);
                } else if (!/^_/.test(prop)) {
                    definition.properties.push(prop);
                }
            }
        );

        Reflect.defineMetadata("puzzl:metaObject", definition, target);

        return target;
    };
}
import "reflect-metadata";

let extend = function (target: Function): Function {
    if (!Reflect.getMetadata("puzzl:extendedConstructor", target)) {
        let f: any = function (...args: Array<any>): any {
            return construct(target, args);
        };

        f.prototype = target.prototype;

        Reflect.defineMetadata("puzzl:metaName", target.name, f);

        function construct(constructor: Function, args: Array<any>): any {
            let c: any  = function (): any {
                let overloadsArgs = Reflect.getMetadata("puzzl:overload:constructor:args", f);
                if (overloadsArgs) {
                    overloadsArgs.forEach(
                        (overload: (args: Array<any>) => Array<any>) => {
                            args = overload(args);
                        }
                    );
                }

                let currentObjectModifiers = Reflect.getMetadata("puzzl:overload:constructor:modifier", f);
                if (currentObjectModifiers) {
                    currentObjectModifiers.forEach(
                        (modifier: (target: any, args: Array<any>) => Array<any>) => {
                            args = modifier(this, args);
                        }
                    );
                }

                let beforeApplyRoutines = Reflect.getMetadata("puzzl:overload:constructor:beforeApplyRoutine", f);
                if (!beforeApplyRoutines) {
                    beforeApplyRoutines = [];
                }

                // console.log(Reflect.getMetadata("puzzl:piece:name", target));
                // if (Reflect.getMetadata("puzzl:piece:name", target) === "FileWatcher") {
                //     console.log(args);
                // }

                let oldP = this.promise;
                this.promise = Promise.each(
                    beforeApplyRoutines, (routine: (target: any) => Promise<any|void>) => {
                        return routine(this);
                    }
                ).then(
                    () => {
                        return new Promise(
                            (resolve: (value?: any) => void, reject: (error: any) => void) => {
                                constructor.apply(this, args);
                                if (global._.isFunction(this.then)) {
                                    this.then(resolve).catch(reject);
                                } else if (oldP && global._.isFunction(oldP.then)) {
                                    oldP.then(resolve).catch(reject);
                                } else {
                                    resolve();
                                }
                            }
                        );
                    }
                ).then(
                    () => {
                        return new Promise(
                            (resolve: (value?: any) => void, reject: (error: any) => void) => {
                                let afterApplyRoutines = Reflect.getMetadata("puzzl:overload:constructor:afterApplyRoutine", f);
                                if (afterApplyRoutines) {
                                    Promise.each(
                                        afterApplyRoutines, (routine: (target: any) => void|Promise<any|void>) => {
                                            return routine(this);
                                        }
                                    ).then(resolve).catch(reject);
                                } else {
                                    resolve();
                                }
                            }
                        );
                    }
                );

                return this;
            };
            c.prototype = constructor.prototype;
            return new c();
        }

        Reflect.defineMetadata("puzzl:extendedConstructor", true, f);
        Reflect.getMetadataKeys(target).forEach(
            (key: string) => {
                Reflect.defineMetadata(key, Reflect.getMetadata(key, target), f);
            }
        );

        return f;
    }

    return target;
};

/**
 * Register a method to overload constructor args
 * @param target
 * @param overload
 * @returns {Function}
 */
let registerOverloadArgs = function (target: Function, overload: (args: Array<any>) => Array<any>): Function {
    let overloads = [];

    if (Reflect.getMetadata("puzzl:overload:constructor:args", target)) {
        overloads = Reflect.getMetadata("puzzl:overload:constructor:args", target);
    }

    overloads.push(overload);

    Reflect.defineMetadata("puzzl:overload:constructor:args", overloads, target);

    return target;
};

/**
 * Register a method to modify the current instance
 * @param target
 * @param modifier
 * @returns {Function}
 */
let registerInstanceModifier = function (target: Function, modifier: (target: any, args: Array<any>) => Array<any>): Function {
    let modifiers = [];

    if (Reflect.getMetadata("puzzl:overload:constructor:modifier", target)) {
        modifiers = Reflect.getMetadata("puzzl:overload:constructor:modifier", target);
    }

    modifiers.push(modifier);

    Reflect.defineMetadata("puzzl:overload:constructor:modifier", modifiers, target);

    return target;
};

/**
 * Register a method to call before the original constructor is applied
 * @param target
 * @param routine
 * @returns {Function}
 */
let registerBeforeApplyRoutine = function (target: Function, routine: (target: any) => void|Promise<any|void>): Function {
    let routines = [];

    if (Reflect.getMetadata("puzzl:overload:constructor:beforeApplyRoutine", target)) {
        routines = Reflect.getMetadata("puzzl:overload:constructor:beforeApplyRoutine", target);
    }

    routines.push(routine);

    Reflect.defineMetadata("puzzl:overload:constructor:beforeApplyRoutine", routines, target);

    return target;
};

/**
 * Register a method to call after the original constructor is applied
 * @param target
 * @param routine
 * @returns {Function}
 */
let registerAfterApplyRoutine = function (target: Function, routine: (target: any) => void|Promise<any|void>): Function {
    let routines = [];

    if (Reflect.getMetadata("puzzl:overload:constructor:afterApplyRoutine", target)) {
        routines = Reflect.getMetadata("puzzl:overload:constructor:afterApplyRoutine", target);
    }

    routines.push(routine);

    Reflect.defineMetadata("puzzl:overload:constructor:afterApplyRoutine", routines, target);

    return target;
};

export {extend, registerOverloadArgs, registerInstanceModifier, registerBeforeApplyRoutine, registerAfterApplyRoutine};
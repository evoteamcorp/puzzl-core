"use strict";
var ExtendedConstructor_1 = require("./ExtendedConstructor");
require("reflect-metadata");
function Instantiate(options) {
    var lodash = global._ || require("lodash");
    options = lodash.merge({ sharedInstance: true }, options) || { sharedInstance: true };
    return function instanciateFactory(target, decoratedPropertyName) {
        target = ExtendedConstructor_1.extend(target);
        Reflect.defineMetadata("puzzl:instantiate", true, target);
        Reflect.defineMetadata("puzzl:instantiate:options", options, target);
        return target;
    };
}
exports.Instantiate = Instantiate;
//# sourceMappingURL=Instantiate.js.map
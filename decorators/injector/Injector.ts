import {MetaObjectDefinition} from "../../manual_typings/built_in";
import {extend, registerOverloadArgs} from "../common/ExtendedConstructor";
import {EventManager} from "../../managers/EventManager";
import "reflect-metadata";

export function Injector(): any {
    return function injectorFactory(target: Function, decoratedPropertyName?: string): any {
        let params = Reflect.getMetadata("design:paramtypes", target);
        target     = extend(target);
        target     = registerOverloadArgs(
            target, (args: Array<any>): Array<any> => {
                let realArgs = [];
                params.forEach(
                    function (param: any, index: number): void {
                        let metaObj: MetaObjectDefinition = Reflect.getMetadata("puzzl:metaObject", param);
                        if (metaObj) {
                            realArgs.push(new InjectorWrapper(metaObj)/*Puzzl.getInjector().get(metaObj.name)*/);
                        } else {
                            if (args[index]) {
                                realArgs.push(args[index]);
                            }
                        }
                    }
                );

                return realArgs;
            }
        );

        return target;
    };
}

class InjectorWrapper {

    private _instance: any = undefined;

    constructor(private _metaObj: any) {
        this._metaObj.functions.forEach(
            (func: string) => {
                Object.defineProperty(
                    this, func, {
                        "get": (): Function => {
                            if (!this._instance) {
                                this.instance = Puzzl.getInjector().get(this._metaObj.name);
                            }
                            if (this._instance) {
                                return (...args: Array<any>) => {
                                    return this._instance[func].apply(this._instance, args);
                                };
                            } else {
                                return undefined;
                            }
                        }
                    }
                );
            }
        );

        this._metaObj.properties.forEach(
            (prop: string) => {
                if (prop !== "promise" && prop !== "domain") {
                    Object.defineProperty(
                        this, prop, {
                            "get": (): Function => {
                                if (!this._instance) {
                                    this.instance = Puzzl.getInjector().get(this._metaObj.name);
                                }
                                if (this._instance) {
                                    return this._instance[prop];
                                } else {
                                    return undefined;
                                }
                            }
                        }
                    );
                }
            }
        );

        let em: EventManager = Puzzl.getInjector().get("EventManager");
        if (em) {
            em.on("puzzl:injector:" + this._metaObj.name + ":registered", () => {
                this.instance = Puzzl.getInjector().get(this._metaObj.name);
            });
        }

        this._checkInstance();
    }

    /**
     * Add passthrough on instance properties
     * @param value
     */
    private set instance(value: any) {
        this._instance = value;
        try {
            if (this._instance) {
                Object.keys(this._instance).forEach(
                    (prop: string) => {
                        if (!/^_/.test(prop)) {
                            this._metaObj.properties.push(prop);
                            Object.defineProperty(
                                this, prop, {
                                    "get": (): Function => {
                                        return this._instance[prop];
                                    }
                                }
                            );
                        }
                    }
                );
            }
        } catch (e) {
        }
    }

    /**
     * Check if an instance is available
     * @private
     */
    private _checkInstance(): void {
        this.instance = Puzzl.getInjector().get(this._metaObj.name);
        if (!this._instance) {
            setTimeout(
                () => {
                    this._checkInstance();
                },
                0
            );
        }
    }
}
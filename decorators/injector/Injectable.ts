///<reference path="../../manual_typings/global.d.ts" />
import {InjectableOptions, InjectableDefinition} from "../../manual_typings/built_in";
import {extend, registerBeforeApplyRoutine, registerAfterApplyRoutine} from "../common/ExtendedConstructor";
import "reflect-metadata";
import path = require("path");

function makeInjectableDecorator(options: InjectableOptions, target: Function, decoratedPropertyName: string): any {
    target = extend(target);

    if (!options.afterInstantiation) {
        registerBeforeApplyRoutine(
            target, (currentTarget: any) => {
                return Promise.each(
                    options.injectables, (injectable: InjectableDefinition) => {
                        return Puzzl.getInjector().load(
                            injectable.category,
                            (
                                injectable.pieceName || Reflect.getMetadata("puzzl:piece:name", target)
                            ),
                            path.resolve(injectable.path),
                            injectable.order
                        );
                    }
                );
            }
        );
    } else {
        registerAfterApplyRoutine(
            target, (currentTarget: any) => {
                return Promise.each(
                    options.injectables, (injectable: InjectableDefinition) => {
                        return Puzzl.getInjector().load(
                            injectable.category,
                            (
                                injectable.pieceName || Reflect.getMetadata("puzzl:piece:name", target)
                            ),
                            path.resolve(injectable.path),
                            injectable.order
                        );
                    }
                );
            }
        );
    }

    return target;
}

export function Injectable(options: InjectableOptions): any {
    return function injectableFactory(target: Function, decoratedPropertyName?: string): any {
        return makeInjectableDecorator(options, target, decoratedPropertyName);
    };
}

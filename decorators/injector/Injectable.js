"use strict";
var ExtendedConstructor_1 = require("../common/ExtendedConstructor");
require("reflect-metadata");
var path = require("path");
function makeInjectableDecorator(options, target, decoratedPropertyName) {
    target = ExtendedConstructor_1.extend(target);
    if (!options.afterInstantiation) {
        ExtendedConstructor_1.registerBeforeApplyRoutine(target, function (currentTarget) {
            return Promise.each(options.injectables, function (injectable) {
                return Puzzl.getInjector().load(injectable.category, (injectable.pieceName || Reflect.getMetadata("puzzl:piece:name", target)), path.resolve(injectable.path), injectable.order);
            });
        });
    }
    else {
        ExtendedConstructor_1.registerAfterApplyRoutine(target, function (currentTarget) {
            return Promise.each(options.injectables, function (injectable) {
                return Puzzl.getInjector().load(injectable.category, (injectable.pieceName || Reflect.getMetadata("puzzl:piece:name", target)), path.resolve(injectable.path), injectable.order);
            });
        });
    }
    return target;
}
function Injectable(options) {
    return function injectableFactory(target, decoratedPropertyName) {
        return makeInjectableDecorator(options, target, decoratedPropertyName);
    };
}
exports.Injectable = Injectable;
//# sourceMappingURL=Injectable.js.map
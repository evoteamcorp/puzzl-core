"use strict";
var ExtendedConstructor_1 = require("../common/ExtendedConstructor");
require("reflect-metadata");
function Injector() {
    return function injectorFactory(target, decoratedPropertyName) {
        var params = Reflect.getMetadata("design:paramtypes", target);
        target = ExtendedConstructor_1.extend(target);
        target = ExtendedConstructor_1.registerOverloadArgs(target, function (args) {
            var realArgs = [];
            params.forEach(function (param, index) {
                var metaObj = Reflect.getMetadata("puzzl:metaObject", param);
                if (metaObj) {
                    realArgs.push(new InjectorWrapper(metaObj));
                }
                else {
                    if (args[index]) {
                        realArgs.push(args[index]);
                    }
                }
            });
            return realArgs;
        });
        return target;
    };
}
exports.Injector = Injector;
var InjectorWrapper = (function () {
    function InjectorWrapper(_metaObj) {
        var _this = this;
        this._metaObj = _metaObj;
        this._instance = undefined;
        this._metaObj.functions.forEach(function (func) {
            Object.defineProperty(_this, func, {
                "get": function () {
                    if (!_this._instance) {
                        _this.instance = Puzzl.getInjector().get(_this._metaObj.name);
                    }
                    if (_this._instance) {
                        return function () {
                            var args = [];
                            for (var _i = 0; _i < arguments.length; _i++) {
                                args[_i - 0] = arguments[_i];
                            }
                            return _this._instance[func].apply(_this._instance, args);
                        };
                    }
                    else {
                        return undefined;
                    }
                }
            });
        });
        this._metaObj.properties.forEach(function (prop) {
            if (prop !== "promise" && prop !== "domain") {
                Object.defineProperty(_this, prop, {
                    "get": function () {
                        if (!_this._instance) {
                            _this.instance = Puzzl.getInjector().get(_this._metaObj.name);
                        }
                        if (_this._instance) {
                            return _this._instance[prop];
                        }
                        else {
                            return undefined;
                        }
                    }
                });
            }
        });
        var em = Puzzl.getInjector().get("EventManager");
        if (em) {
            em.on("puzzl:injector:" + this._metaObj.name + ":registered", function () {
                _this.instance = Puzzl.getInjector().get(_this._metaObj.name);
            });
        }
        this._checkInstance();
    }
    Object.defineProperty(InjectorWrapper.prototype, "instance", {
        set: function (value) {
            var _this = this;
            this._instance = value;
            try {
                if (this._instance) {
                    Object.keys(this._instance).forEach(function (prop) {
                        if (!/^_/.test(prop)) {
                            _this._metaObj.properties.push(prop);
                            Object.defineProperty(_this, prop, {
                                "get": function () {
                                    return _this._instance[prop];
                                }
                            });
                        }
                    });
                }
            }
            catch (e) {
            }
        },
        enumerable: true,
        configurable: true
    });
    InjectorWrapper.prototype._checkInstance = function () {
        var _this = this;
        this.instance = Puzzl.getInjector().get(this._metaObj.name);
        if (!this._instance) {
            setTimeout(function () {
                _this._checkInstance();
            }, 0);
        }
    };
    return InjectorWrapper;
}());
//# sourceMappingURL=Injector.js.map
"use strict";
var ExtendedConstructor_1 = require("../common/ExtendedConstructor");
require("reflect-metadata");
function makePieceDecorator(target, decoratedPropertyName) {
    target = ExtendedConstructor_1.extend(target);
    target = ExtendedConstructor_1.registerInstanceModifier(target, function (instance, args) {
        instance.promise = new Promise(function (resolve, reject) {
            args.unshift(reject);
            args.unshift(resolve);
        });
        return args;
    });
    return target;
}
function Piece(options) {
    options = options || {};
    return function pieceFactory(target, decoratedPropertyName) {
        Reflect.defineMetadata("puzzl:piece:name", options.name || target.name || Reflect.getMetadata("puzzl:metaName", target), target);
        return makePieceDecorator(target, decoratedPropertyName);
    };
}
exports.Piece = Piece;
//# sourceMappingURL=Piece.js.map
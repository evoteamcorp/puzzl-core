///<reference path="../../manual_typings/global.d.ts" />
import {PieceOptions} from "../../manual_typings/built_in";
import {extend, registerInstanceModifier} from "../common/ExtendedConstructor";
import "reflect-metadata";

function makePieceDecorator(target: Function, decoratedPropertyName: string): any {
    target = extend(target);
    target = registerInstanceModifier(target, (instance: any, args: Array<any>) => {
        instance.promise = new Promise((resolve: (value?: any) => void, reject: (error: any) => void) => {
            args.unshift(reject);
            args.unshift(resolve);
        });

        return args;
    });

    return target;
}

export function Piece(options?: PieceOptions): any {
    options = options || {};
    return function pieceFactory(target: Function, decoratedPropertyName?: string): any {
        Reflect.defineMetadata("puzzl:piece:name", options.name || target.name || Reflect.getMetadata("puzzl:metaName", target), target);
        return makePieceDecorator(target, decoratedPropertyName);
    };
}

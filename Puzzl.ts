import {Core} from "./internals/Core";
export class Bootstrap {

    private _core: Core = null;

    constructor(onReady: () => void, onError: (error?: any) => void) {
        global.Promise = require("bluebird");
        global._       = require("lodash");
        this._core     = new Core();
        this._core.promise.then(
            () => {
                return new Promise(
                    (resolve: (value?: any) => void, reject: (error: any) => void) => {
                        this._core.initialize(resolve, reject);
                    }
                );
            }
        ).then(onReady).catch(onError);
    }
}
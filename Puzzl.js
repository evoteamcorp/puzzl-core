"use strict";
var Core_1 = require("./internals/Core");
var Bootstrap = (function () {
    function Bootstrap(onReady, onError) {
        var _this = this;
        this._core = null;
        global.Promise = require("bluebird");
        global._ = require("lodash");
        this._core = new Core_1.Core();
        this._core.promise.then(function () {
            return new Promise(function (resolve, reject) {
                _this._core.initialize(resolve, reject);
            });
        }).then(onReady).catch(onError);
    }
    return Bootstrap;
}());
exports.Bootstrap = Bootstrap;
//# sourceMappingURL=Puzzl.js.map
///<reference path="../typings/index.d.ts" />
///<reference path="../node_modules/reflect-metadata/reflect-metadata.d.ts" />
declare interface InjectorFactoryInternal {
    load(category: string, pieceName: string, path: string, order?: Array<string>): Promise<Array<string>>;
    get(name: string, pieceName?: string|Array<string>): any;
}

declare interface Core {
    getInjector(): InjectorFactoryInternal;
}

declare let Puzzl: Core;

declare namespace NodeJS {
    export interface Global {
        Puzzl: Core;
        _: any;
    }
}

declare interface Function {
    name: string;
}
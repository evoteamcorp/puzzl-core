import {IPiece} from "../interfaces/IPiece";
export interface PathsDictionary {
    [index: string]: string;
}

export interface InjectableOptions {
    afterInstantiation: boolean;
    injectables: Array<InjectableDefinition>;
}

export interface InjectableDefinition {
    category: string;
    path: string;
    pieceName?: string;
    order: [string];
}

export interface PieceOptions {
    name?: string;
    asStart?: boolean;
}

export interface InstantiateOptions {
    sharedInstance?: boolean;
    afterInstantiate?: Array<string>|string;
}

export interface MetaObjectDefinition {
    name?: string;
    functions?: Array<string>;
    properties?: Array<string>;
}

export interface PuzzlErrorStore {
    [index: string]: string;
}

export interface PieceDictionary {
    [index: string]: IPiece;
}
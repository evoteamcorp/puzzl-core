"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var PackageJsonCore_1 = require("../cores/PackageJsonCore");
var LoggerCore_1 = require("../cores/LoggerCore");
var ConfigsCore_1 = require("../cores/ConfigsCore");
var LoaderCore_1 = require("../cores/LoaderCore");
var PathsUtil_1 = require("../utils/PathsUtil");
var FileUtil_1 = require("../utils/FileUtil");
var Injector_1 = require("../decorators/injector/Injector");
var MetaObject_1 = require("../decorators/common/MetaObject");
var Instantiate_1 = require("../decorators/common/Instantiate");
var EventManager_1 = require("./EventManager");
var PieceManager = (function () {
    function PieceManager(_packageJsonCore, _logger, _configsCore, _loaderCore, _pathsUtil, _fileUtil, _eventManager) {
        this._packageJsonCore = _packageJsonCore;
        this._logger = _logger;
        this._configsCore = _configsCore;
        this._loaderCore = _loaderCore;
        this._pathsUtil = _pathsUtil;
        this._fileUtil = _fileUtil;
        this._eventManager = _eventManager;
        this._registeredObjects = {};
        this._instanceObjects = {};
        this._instantiatedPieces = [];
        this._initializedPieces = [];
    }
    PieceManager.prototype.initialize = function () {
        var _this = this;
        var pieces = this._packageJsonCore.getPiecesMatchingPattern(/^puzzl-/, "App");
        if (pieces.indexOf("puzzl-core") !== -1) {
            delete pieces[pieces.indexOf("puzzl-core")];
        }
        this._logger.debug("Detected Modules :", pieces);
        pieces = this.orderPieces(pieces);
        this._logger.debug("Detected Modules Order:", pieces);
        var pieceLoadEachCallback = function (pieceName) {
            return _this.instantiatePiece(pieceName);
        };
        var initializePieces = function () {
            var initializePiece = function (pieceName) {
                return _this.initializePiece(pieceName);
            };
            return Promise.each(_this._instantiatedPieces, initializePiece);
        };
        return Promise.each(pieces, pieceLoadEachCallback).then(initializePieces);
    };
    PieceManager.prototype.isInstantiated = function (pieceName) {
        return this._instantiatedPieces.indexOf(pieceName) !== -1;
    };
    PieceManager.prototype.isLoaded = function (pieceName) {
        return this._initializedPieces.indexOf(pieceName) !== -1;
    };
    PieceManager.prototype.getModule = function (pieceName) {
        if (pieceName in this._instanceObjects) {
            return this._instanceObjects[pieceName];
        }
        else {
            return null;
        }
    };
    PieceManager.prototype.orderPieces = function (pieceNames) {
        var config = this._configsCore.get("PiecesOrder");
        var order = [];
        if (config) {
            config.forEach(function (pieceName) {
                if (pieceNames.indexOf(pieceName) !== -1) {
                    order.push(pieceName);
                }
            });
        }
        pieceNames.forEach(function (pieceName) {
            if (order.indexOf(pieceName) === -1) {
                order.push(pieceName);
            }
        });
        return order;
    };
    PieceManager.prototype.instantiatePiece = function (pieceName) {
        var _this = this;
        var attemps = ["./", "../", "../../", "../../../", "../../../../", "../../../../../"];
        var foundPath = null;
        return Promise.each(attemps, function (relPath) {
            var cPath = _this._pathsUtil.getApp(relPath + "node_modules");
            return _this._fileUtil.dirExist(cPath).then(function (result) {
                if (result) {
                    foundPath = cPath;
                }
            });
        }).then(function () {
            var piecePath = foundPath + "/" + pieceName;
            _this._logger.debug("Load module :", pieceName, piecePath);
            var pieceAddFunction = function () {
                var args = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    args[_i - 0] = arguments[_i];
                }
                return _this.registerPiece.apply(_this, args);
            };
            return _this._loaderCore.require(piecePath, pieceName, pieceAddFunction);
        });
    };
    PieceManager.prototype.registerPiece = function (className, pieceName, pieceObject) {
        var _this = this;
        var promise = function (resolve, reject) {
            className = className.replace("Puzzl", "");
            _this._registeredObjects[className] = {
                className: className,
                moduleName: pieceName,
                object: pieceObject
            };
            _this._logger.debug("Register piece :", className, pieceName, pieceObject);
            _this._eventManager.once("piece:" + className + ":instantiated", function () {
                if (_this._instantiatedPieces.indexOf(className) === -1) {
                    _this._instantiatedPieces.push(className);
                }
                _this._logger.debug("Piece " + className + " instantiated");
            });
            _this._eventManager.once("piece:" + className + ":initialized", function () {
                if (_this._initializedPieces.indexOf(className) === -1) {
                    _this._initializedPieces.push(className);
                }
                _this._logger.debug("Piece " + className + " initialized");
            });
            _this._instanceObjects[className] = new pieceObject();
            _this._pathsUtil.addPiecePath(className, _this._instanceObjects[className].getPath());
            _this._instanceObjects[className].promise.then(function () {
                _this._eventManager.emit("piece:" + className + ":instantiated", _this._instanceObjects[className]);
                resolve();
            }).catch(reject);
        };
        return new Promise(promise);
    };
    PieceManager.prototype.initializePiece = function (className) {
        var _this = this;
        var promiseInitializePiece = function (resolve, reject) {
            if (className in _this._instanceObjects) {
                try {
                    _this._logger.debug("Call initialize of piece: " + className);
                    var promise = new Promise(function (resolveInit, rejectInit) {
                        _this._instanceObjects[className].initialize(resolveInit, rejectInit);
                    });
                    promise.then(function () {
                        _this._eventManager.emit("piece:" + className + ":initialized", _this._instanceObjects[className]);
                        resolve();
                    }).catch(reject);
                }
                catch (e) {
                    reject(e);
                }
            }
            else {
                _this._logger.warn("Unable to initialize uninstantiated piece : " + className);
                resolve();
            }
        };
        return new Promise(promiseInitializePiece);
    };
    PieceManager = __decorate([
        Injector_1.Injector(),
        Instantiate_1.Instantiate({
            afterInstantiate: "initialize"
        }),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [PackageJsonCore_1.PackageJsonCore, LoggerCore_1.LoggerCore, ConfigsCore_1.ConfigsCore, LoaderCore_1.LoaderCore, PathsUtil_1.PathsUtil, FileUtil_1.FileUtil, EventManager_1.EventManager])
    ], PieceManager);
    return PieceManager;
}());
exports.PieceManager = PieceManager;
//# sourceMappingURL=PieceManager.js.map
"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var events_1 = require("events");
var Instantiate_1 = require("../decorators/common/Instantiate");
var LoggerCore_1 = require("../cores/LoggerCore");
var Injector_1 = require("../decorators/injector/Injector");
var MetaObject_1 = require("../decorators/common/MetaObject");
var EventManager = (function (_super) {
    __extends(EventManager, _super);
    function EventManager(_logger) {
        _super.call(this);
        this._logger = _logger;
        this.setMaxListeners(999999999);
    }
    EventManager.prototype.initialize = function () {
        return new Promise(function (resolve, reject) {
            resolve();
        });
    };
    EventManager.prototype.emit = function (event) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        var realArgs = [];
        args.forEach(function (arg) {
            if (arg.constructor && arg.constructor.name !== "Object") {
                realArgs.push("Instance of " + arg.constructor.name);
            }
            else {
                realArgs.push(arg);
            }
        });
        if (this._logger.debug) {
            this._logger.debug("Emit event:", event, "with datas:", realArgs);
        }
        else {
            console.log("Emit event:", event, "with datas:", realArgs);
        }
        args.unshift(event);
        return _super.prototype.emit.apply(this, args);
    };
    EventManager.prototype.on = function (event) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (this._logger.debug) {
            this._logger.debug("Bind event:", event);
        }
        args.unshift(event);
        return _super.prototype.on.apply(this, args);
    };
    EventManager.prototype.addListener = function (event) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (this._logger.debug) {
            this._logger.debug("Add listener on event:", event);
        }
        args.unshift(event);
        return _super.prototype.addListener.apply(this, args);
    };
    EventManager.prototype.once = function (event) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (this._logger.debug) {
            this._logger.debug("Bind once event:", event);
        }
        args.unshift(event);
        return _super.prototype.once.apply(this, args);
    };
    EventManager.prototype.removeListener = function (event) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (this._logger.debug) {
            this._logger.debug("Unbind event:", event);
        }
        args.unshift(event);
        return _super.prototype.removeListener.apply(this, args);
    };
    EventManager.prototype.removeAllListeners = function (event) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (this._logger.debug) {
            this._logger.debug("Remove all listeners of event:", event);
        }
        args.unshift(event);
        return _super.prototype.removeAllListeners.apply(this, args);
    };
    EventManager.prototype.setMaxListeners = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        if (this._logger.debug) {
            this._logger.debug("Set max listeners:", args);
        }
        return _super.prototype.setMaxListeners.apply(this, args);
    };
    EventManager.prototype.getMaxListeners = function () {
        return _super.prototype.getMaxListeners.apply(this);
    };
    EventManager.prototype.listeners = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        return _super.prototype.getMaxListeners.apply(this, args);
    };
    EventManager.prototype.listenerCount = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        return _super.prototype.listenerCount.apply(this, args);
    };
    EventManager = __decorate([
        Injector_1.Injector(),
        Instantiate_1.Instantiate(),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [LoggerCore_1.LoggerCore])
    ], EventManager);
    return EventManager;
}(events_1.EventEmitter));
exports.EventManager = EventManager;
//# sourceMappingURL=EventManager.js.map
import {EventEmitter} from "events";
import {IManager} from "../interfaces/IManager";
import {Instantiate} from "../decorators/common/Instantiate";
import {LoggerCore} from "../cores/LoggerCore";
import {Injector} from "../decorators/injector/Injector";
import {MetaObject} from "../decorators/common/MetaObject";

@Injector()
@Instantiate()
@MetaObject()

export class EventManager extends EventEmitter implements IManager {

    constructor(private _logger: LoggerCore) {
        super();
        this.setMaxListeners(999999999);
    }

    /**
     * Initialize event
     * @returns {Promise<any>}
     */
    public initialize(): Promise<void> {
        return new Promise(
            (resolve: (value?: any) => void, reject: (error: any) => void) => {
                resolve();
            }
        );
    }

    /**
     * Overload emit to debug
     * @param event
     * @param args
     * @return {boolean}
     */
    public emit(event: string, ...args: Array<any>): boolean {
        let realArgs = [];
        args.forEach((arg: any) => {
            if (arg.constructor && arg.constructor.name !== "Object") {
                realArgs.push("Instance of " + arg.constructor.name);
            } else {
                realArgs.push(arg);
            }
        });
        if (this._logger.debug) {
            this._logger.debug("Emit event:", event, "with datas:", realArgs);
        } else {
            console.log("Emit event:", event, "with datas:", realArgs);
        }
        args.unshift(event);
        return super.emit.apply(this, args);
    }

    /**
     * Overload on to debug
     * @param event
     * @param args
     * @return {EventManager}
     */
    public on(event: string, ...args: Array<any>): this {
        if (this._logger.debug) {
            this._logger.debug("Bind event:", event);
        }
        args.unshift(event);
        return super.on.apply(this, args);
    }

    /**
     * Overload addListener to debug
     * @param event
     * @param args
     * @return {EventManager}
     */
    public addListener(event: string, ...args: Array<any>): this {
        if (this._logger.debug) {
            this._logger.debug("Add listener on event:", event);
        }
        args.unshift(event);
        return super.addListener.apply(this, args);
    }

    /**
     * Overload once to debug
     * @param event
     * @param args
     * @return {EventManager}
     */
    public once(event: string, ...args: Array<any>): this {
        if (this._logger.debug) {
            this._logger.debug("Bind once event:", event);
        }
        args.unshift(event);
        return super.once.apply(this, args);
    }

    /**
     * Overload removeListener to debug
     * @param event
     * @param args
     * @return {EventManager}
     */
    public removeListener(event: string, ...args: Array<any>): this {
        if (this._logger.debug) {
            this._logger.debug("Unbind event:", event);
        }
        args.unshift(event);
        return super.removeListener.apply(this, args);
    }

    /**
     * Overload removeAllListeners to debug
     * @param event
     * @param args
     * @return {EventManager}
     */
    public removeAllListeners(event: string, ...args: Array<any>): this {
        if (this._logger.debug) {
            this._logger.debug("Remove all listeners of event:", event);
        }
        args.unshift(event);
        return super.removeAllListeners.apply(this, args);
    }

    /**
     * Overload setMaxListeners to debug
     * @param args
     * @return {EventManager}
     */
    public setMaxListeners(...args: Array<any>): this {
        if (this._logger.debug) {
            this._logger.debug("Set max listeners:", args);
        }
        return super.setMaxListeners.apply(this, args);
    }

    /**
     * Overload getMaxListeners to metaObj
     * @returns {number}
     */
    public getMaxListeners(): number {
        return super.getMaxListeners.apply(this);
    }

    /**
     * Overload listeners to metaObj
     * @returns {Function[]}
     */
    public listeners(...args: Array<any>): Function[] {
        return super.getMaxListeners.apply(this, args);
    }

    /**
     * Overload listenerCount to metaObj
     * @returns {number}
     */
    public listenerCount(...args: Array<any>): number {
        return super.listenerCount.apply(this, args);
    }

}

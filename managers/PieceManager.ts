import {IManager} from "../interfaces/IManager";
import {PackageJsonCore} from "../cores/PackageJsonCore";
import {LoggerCore} from "../cores/LoggerCore";
import {ConfigsCore} from "../cores/ConfigsCore";
import {LoaderCore} from "../cores/LoaderCore";
import {PathsUtil} from "../utils/PathsUtil";
import {FileUtil} from "../utils/FileUtil";
import {Injector} from "../decorators/injector/Injector";
import {MetaObject} from "../decorators/common/MetaObject";
import {Instantiate} from "../decorators/common/Instantiate";
import {IPiece, CPiece} from "../interfaces/IPiece";
import {EventManager} from "./EventManager";
import {PieceDictionary} from "../manual_typings/built_in";

@Injector()
@Instantiate(
    {
        afterInstantiate: "initialize"
    }
)
@MetaObject()

export class PieceManager implements IManager {

    // Store
    private _registeredObjects: any           = {};
    private _instanceObjects: PieceDictionary = {};

    private _instantiatedPieces: Array<string> = [];
    private _initializedPieces: Array<string>  = [];

    constructor(
        private _packageJsonCore: PackageJsonCore,
        private _logger: LoggerCore,
        private _configsCore: ConfigsCore,
        private _loaderCore: LoaderCore,
        private _pathsUtil: PathsUtil,
        private _fileUtil: FileUtil,
        private _eventManager: EventManager
    ) {
    }

    /**
     * Load Pieces
     * @returns {Promise<void>}
     */
    public initialize(): Promise<Array<string>> {
        let pieces = this._packageJsonCore.getPiecesMatchingPattern(/^puzzl-/, "App");
        if (pieces.indexOf("puzzl-core") !== -1) {
            delete pieces[pieces.indexOf("puzzl-core")];
        }
        this._logger.debug("Detected Modules :", pieces);
        pieces = this.orderPieces(pieces);
        this._logger.debug("Detected Modules Order:", pieces);

        let pieceLoadEachCallback = (pieceName: string): Promise<void> => {
            return this.instantiatePiece(pieceName);
        };

        let initializePieces = (): Promise<Array<string>> => {
            let initializePiece = (pieceName: string): Promise<void> => {
                return this.initializePiece(pieceName);
            };
            return Promise.each(this._instantiatedPieces, initializePiece);
        };

        return Promise.each(pieces, pieceLoadEachCallback).then(initializePieces);
    }

    /**
     * Return if the piece is already instantiated
     * @param pieceName
     * @returns {boolean}
     */
    public isInstantiated(pieceName: string): boolean {
        return this._instantiatedPieces.indexOf(pieceName) !== -1;
    }

    /**
     * Return if the piece is already initialized
     * @param pieceName
     * @returns {boolean}
     */
    public isLoaded(pieceName: string): boolean {
        return this._initializedPieces.indexOf(pieceName) !== -1;
    }

    /**
     * Return instance of given piece
     * @param pieceName
     * @returns {IPiece}
     */
    public getModule(pieceName: string): IPiece {
        if (pieceName in this._instanceObjects) {
            return this._instanceObjects[pieceName];
        } else {
            return null;
        }
    }

    /**
     * Order pieces
     * @param pieceNames
     * @returns {Array}
     */
    private orderPieces(pieceNames: Array<string>): Array<string> {
        let config = this._configsCore.get("PiecesOrder");
        let order  = [];

        if (config) {
            config.forEach(
                function (pieceName: string): void {
                    if (pieceNames.indexOf(pieceName) !== -1) {
                        order.push(pieceName);
                    }
                }
            );
        }

        pieceNames.forEach(
            function (pieceName: string): void {
                if (order.indexOf(pieceName) === -1) {
                    order.push(pieceName);
                }
            }
        );

        return order;
    }

    /**
     * Load a piece
     * @param pieceName
     * @returns {Promise<*>|any}
     */
    private instantiatePiece(pieceName: string): Promise<any> {
        let attemps   = ["./", "../", "../../", "../../../", "../../../../", "../../../../../"];
        let foundPath = null;

        return Promise.each(
            attemps, (relPath: string) => {
                let cPath = this._pathsUtil.getApp(relPath + "node_modules");
                return this._fileUtil.dirExist(cPath).then(
                    (result: boolean) => {
                        if (result) {
                            foundPath = cPath;
                        }
                    }
                );
            }
        ).then(
            () => {
                let piecePath = foundPath + "/" + pieceName;
                this._logger.debug("Load module :", pieceName, piecePath);

                let pieceAddFunction = (...args: Array<any>): Promise<void> => {
                    return this.registerPiece.apply(this, args);
                };

                return this._loaderCore.require(piecePath, pieceName, pieceAddFunction);
            }
        );
    }

    /**
     * Register a piece
     * @param className
     * @param pieceName
     * @param pieceObject
     * @return {Promise<T>|Promise}
     */
    private registerPiece(className: string, pieceName: string, pieceObject: CPiece): Promise<any> {
        let promise = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            className = className.replace("Puzzl", "");

            this._registeredObjects[className] = {
                className : className,
                moduleName: pieceName,
                object    : pieceObject
            };

            this._logger.debug("Register piece :", className, pieceName, pieceObject);
            this._eventManager.once(
                "piece:" + className + ":instantiated", () => {
                    if (this._instantiatedPieces.indexOf(className) === -1) {
                        this._instantiatedPieces.push(className);
                    }
                    this._logger.debug("Piece " + className + " instantiated");
                }
            );

            this._eventManager.once(
                "piece:" + className + ":initialized", () => {
                    if (this._initializedPieces.indexOf(className) === -1) {
                        this._initializedPieces.push(className);
                    }
                    this._logger.debug("Piece " + className + " initialized");
                }
            );

            this._instanceObjects[className] = new pieceObject();
            this._pathsUtil.addPiecePath(className, this._instanceObjects[className].getPath());
            this._instanceObjects[className].promise.then(
                () => {
                    this._eventManager.emit("piece:" + className + ":instantiated", this._instanceObjects[className]);
                    resolve();
                }
            ).catch(reject);
        };

        return new Promise(promise);
    }

    /**
     * Initialize piece
     * @param className
     * @returns {Promise<T>|Promise}
     * @private
     */
    private initializePiece(className: string): Promise<void> {
        let promiseInitializePiece = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            if (className in this._instanceObjects) {
                try {
                    this._logger.debug("Call initialize of piece: " + className);
                    let promise = new Promise(
                        (resolveInit: (value?: any) => void, rejectInit: (error: any) => void) => {
                            this._instanceObjects[className].initialize(resolveInit, rejectInit);
                        }
                    );
                    promise.then(
                        () => {
                            this._eventManager.emit("piece:" + className + ":initialized", this._instanceObjects[className]);
                            resolve();
                        }
                    ).catch(reject);
                } catch (e) {
                    reject(e);
                }
            } else {
                this._logger.warn("Unable to initialize uninstantiated piece : " + className);
                resolve();
            }
        };

        return new Promise(promiseInitializePiece);
    }
}

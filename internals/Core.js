"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var InjectorFactoryInternal_1 = require("./InjectorFactoryInternal");
var Injectable_1 = require("./../decorators/injector/Injectable");
var Piece_1 = require("../decorators/piece/Piece");
var MetaObject_1 = require("../decorators/common/MetaObject");
require("source-map-support").install();
var Core = (function () {
    function Core(instanciated, error) {
        this._injector = null;
        this._injector = new InjectorFactoryInternal_1.InjectorFactoryInternal();
        global.Puzzl = this;
        instanciated();
    }
    Core.prototype.initialize = function (initialized, error) {
        initialized();
    };
    Core.prototype.getInjector = function () {
        return this._injector;
    };
    Core.prototype.getPath = function () {
        return __dirname;
    };
    Core = __decorate([
        Injectable_1.Injectable({
            afterInstantiation: true,
            injectables: [
                {
                    category: "Manager",
                    order: ["EventManager"],
                    path: __dirname + "/../managers"
                },
                {
                    category: "Util",
                    path: __dirname + "/../utils"
                }, {
                    category: "Core",
                    order: [
                        "LoggerCore",
                        "LoaderCore",
                        "ConfigsCore",
                        "*"
                    ],
                    path: __dirname + "/../cores"
                },
                {
                    category: "Manager",
                    path: __dirname + "/../managers"
                }
            ]
        }),
        MetaObject_1.MetaObject(),
        Piece_1.Piece(), 
        __metadata('design:paramtypes', [Function, Function])
    ], Core);
    return Core;
}());
exports.Core = Core;
//# sourceMappingURL=Core.js.map
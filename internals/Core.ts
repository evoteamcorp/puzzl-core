import {InjectorFactoryInternal} from "./InjectorFactoryInternal";
import {Injectable} from "./../decorators/injector/Injectable";
import {Piece} from "../decorators/piece/Piece";
import {InjectableOptions, InjectableDefinition} from "../manual_typings/built_in";
import {IPiece} from "../interfaces/IPiece";
import {MetaObject} from "../decorators/common/MetaObject";
require("source-map-support").install();

@Injectable(
    {
        afterInstantiation: true,
        injectables       : [
            {
                category: "Manager",
                order   : ["EventManager"],
                path    : __dirname + "/../managers"
            },
            {
                category: "Util",
                path    : __dirname + "/../utils"
            }, {
                category: "Core",
                order   : [
                    "LoggerCore",
                    "LoaderCore",
                    "ConfigsCore",
                    "*"
                ],
                path    : __dirname + "/../cores"
            },
            {
                category: "Manager",
                path    : __dirname + "/../managers"
            }
        ] as Array<InjectableDefinition>
    } as InjectableOptions
)

@MetaObject()
@Piece()

export class Core implements IPiece {

    public promise: Promise<void>;

    private _injector: InjectorFactoryInternal = null;

    constructor(instanciated?: (value?: any) => void, error?: (error: any) => void) {
        this._injector = new InjectorFactoryInternal();
        global.Puzzl   = this;
        instanciated();
    }

    /**
     * Initialize the piece
     * @param initialized
     * @param error
     */
    public initialize(initialized: (value?: any) => void, error?: (error: any) => void): void {
        initialized();
    }

    /**
     * Return the injector factory
     * @returns {InjectorFactoryInternal}
     */
    public getInjector(): InjectorFactoryInternal {
        return this._injector;
    }

    /**
     * Return the piece path
     * @returns {string}
     */
    public getPath(): string {
        return __dirname;
    }
}
"use strict";
var LoaderCore_1 = require("../cores/LoaderCore");
var FileUtil_1 = require("../utils/FileUtil");
var StringUtil_1 = require("../utils/StringUtil");
require("reflect-metadata");
var InjectorFactoryInternal = (function () {
    function InjectorFactoryInternal() {
        this._cache = {};
    }
    InjectorFactoryInternal.prototype.register = function (category, name, piece, obj) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            if (!(piece in _this._cache)) {
                _this._cache[piece] = {};
            }
            if (!(category in _this._cache[piece])) {
                _this._cache[piece][category] = {};
            }
            if (!_this._cache[piece][category][name]) {
                var options_1 = Reflect.getMetadata("puzzl:instantiate:options", obj);
                if (Reflect.getMetadata("puzzl:instantiate", obj) && options_1 && options_1.sharedInstance) {
                    _this._cache[piece][category][name] = new obj();
                    if (_this._cache[piece][category][name].promise) {
                        _this._cache[piece][category][name].promise.then(function () {
                            if (options_1.afterInstantiate && options_1.afterInstantiate.length) {
                                return _this._executeAfterInstantiate(options_1.afterInstantiate, _this._cache[piece][category][name]);
                            }
                            else {
                                return new Promise(function (resolveInstantiate, rejectInstantiate) {
                                    resolveInstantiate();
                                });
                            }
                        }).then(resolve).catch(reject);
                    }
                    else {
                        resolve();
                    }
                }
                else {
                    _this._cache[piece][category][name] = obj;
                    resolve();
                }
            }
            else {
                resolve();
            }
        }).then(function () {
            return new Promise(function (resolve) {
                if (_this.get("EventManager")) {
                    _this.get("EventManager").emit("puzzl:injector:" + name + ":registered");
                }
                resolve();
            });
        });
    };
    InjectorFactoryInternal.prototype.load = function (category, pieceName, path, order, fromEvent) {
        var _this = this;
        var loader = this.get("LoaderCore");
        var fileUtil = this.get("FileUtil");
        if (!loader) {
            loader = new LoaderCore_1.LoaderCore();
        }
        if (!fileUtil) {
            fileUtil = new FileUtil_1.FileUtil();
        }
        return fileUtil.getFilesInDir(path)
            .then(function (files) {
            var filesPath = [];
            var orderedFiles = [];
            return new Promise(function (resolve, reject) {
                if (order && order.length) {
                    Promise.each(order, function (file) {
                        return new Promise(function (resolveOrder, rejectOrder) {
                            files.forEach(function (fileName) {
                                if (file === "*") {
                                    if (orderedFiles.indexOf(fileName) === -1) {
                                        orderedFiles.push(fileName);
                                    }
                                }
                                else if ((new RegExp(file)).test(fileName)) {
                                    orderedFiles.push(fileName);
                                }
                            });
                            resolveOrder();
                        });
                    }).then(resolve).catch(reject);
                }
                else {
                    orderedFiles = files;
                    resolve();
                }
            }).then(function () {
                return Promise.each(orderedFiles, function (file) {
                    filesPath.push(path + "/" + file);
                });
            }).then(function () {
                return loader.requireFiles(filesPath, pieceName, function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i - 0] = arguments[_i];
                    }
                    args.unshift(category);
                    return _this.register.apply(_this, args);
                });
            });
        });
    };
    InjectorFactoryInternal.prototype.get = function (name, pieceName) {
        if (pieceName === void 0) { pieceName = ["App", "Core"]; }
        if (typeof pieceName === "string") {
            pieceName = [pieceName];
        }
        var stringUtil;
        if (!this._cache.hasOwnProperty("Core") ||
            !this._cache.Core.hasOwnProperty("Util") ||
            !this._cache.Core.Util.hasOwnProperty("StringUtil")) {
            stringUtil = new StringUtil_1.StringUtil();
        }
        else {
            stringUtil = this._cache.Core.Util.StringUtil;
        }
        var category = stringUtil.ucfirst(stringUtil.uncamelize(name).split("_").pop());
        for (var i = 0; i < pieceName.length; i++) {
            if (pieceName[i] in
                this._cache &&
                category in
                    this._cache[pieceName[i]] &&
                name in
                    this._cache[pieceName[i]][category]) {
                return this._cache[pieceName[i]][category][name];
            }
        }
        var pieceNames = Object.keys(this._cache);
        for (var i = 0; i < pieceNames.length; i++) {
            var currentPieceName = pieceNames[i];
            var categories = Object.keys(this._cache[currentPieceName]);
            for (var z = 0; z < categories.length; z++) {
                var currentCategory = categories[z];
                if (currentPieceName in this._cache &&
                    currentCategory in this._cache[currentPieceName] &&
                    name in this._cache[currentPieceName][currentCategory]) {
                    return this._cache[currentPieceName][currentCategory][name];
                }
            }
        }
        return null;
    };
    InjectorFactoryInternal.prototype._executeAfterInstantiate = function (afterInstantiate, obj) {
        if (global._.isString(afterInstantiate)) {
            afterInstantiate = [afterInstantiate];
        }
        return Promise.each(afterInstantiate, function (action) {
            return obj[action]();
        });
    };
    return InjectorFactoryInternal;
}());
exports.InjectorFactoryInternal = InjectorFactoryInternal;
//# sourceMappingURL=InjectorFactoryInternal.js.map
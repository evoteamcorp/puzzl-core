import {LoaderCore} from "../cores/LoaderCore";
import {FileUtil} from "../utils/FileUtil";
import {StringUtil} from "../utils/StringUtil";
import {InstantiateOptions} from "../manual_typings/built_in";
import "reflect-metadata";

export class InjectorFactoryInternal {

    // Dependency store
    private _cache: any = {};

    constructor() {
    }

    /**
     * Register an object into the injector factory
     * @param category
     * @param piece
     * @param name
     * @param obj
     */
    public register(category: string, name: string, piece: string, obj: any): Promise<any> {
        return new Promise(
            (resolve: (value?: any) => void, reject: (error: any) => void) => {
                if (!(
                        piece in this._cache
                    )) {
                    this._cache[piece] = {};
                }

                if (!(
                        category in this._cache[piece]
                    )) {
                    this._cache[piece][category] = {};
                }

                if (!this._cache[piece][category][name]) {
                    let options: InstantiateOptions = Reflect.getMetadata("puzzl:instantiate:options", obj);
                    if (Reflect.getMetadata("puzzl:instantiate", obj) && options && options.sharedInstance) {
                        this._cache[piece][category][name] = new obj();
                        if (this._cache[piece][category][name].promise) {
                            this._cache[piece][category][name].promise.then(
                                () => {
                                    if (options.afterInstantiate && options.afterInstantiate.length) {
                                        return this._executeAfterInstantiate(options.afterInstantiate, this._cache[piece][category][name]);
                                    } else {
                                        return new Promise(
                                            (resolveInstantiate: (value?: any) => void, rejectInstantiate: (error: any) => void) => {
                                                resolveInstantiate();
                                            }
                                        );
                                    }
                                }
                            ).then(resolve).catch(reject);
                        } else {
                            resolve();
                        }
                    } else {
                        this._cache[piece][category][name] = obj;
                        resolve();
                    }
                } else {
                    resolve();
                }
            }
        ).then(
            () => {
                return new Promise(
                    (resolve: () => void) => {
                        if (this.get("EventManager")) {
                            this.get("EventManager").emit("puzzl:injector:" + name + ":registered");
                        }
                        resolve();
                    }
                );
            }
        );
    }

    /**
     * Load objects
     * @returns {Promise<any>}
     */
    public load(category: string, pieceName: string, path: string, order?: Array<string>, fromEvent?: boolean): Promise<Array<string>> {
        let loader: LoaderCore = this.get("LoaderCore");
        let fileUtil: FileUtil = this.get("FileUtil");
        if (!loader) {
            loader = new LoaderCore();
        }
        if (!fileUtil) {
            fileUtil = new FileUtil();
        }

        return fileUtil.getFilesInDir(path)
                       .then(
                           (files: Array<string>) => {
                               let filesPath    = [];
                               let orderedFiles = [];
                               return new Promise(
                                   (resolve: (value?: any) => void, reject: (error: any) => void) => {
                                       if (order && order.length) {
                                           Promise.each(
                                               order, (file: string) => {
                                                   return new Promise(
                                                       (
                                                           resolveOrder: (value?: any) => void,
                                                           rejectOrder: (error: any) => void
                                                       ) => {
                                                           files.forEach(
                                                               (fileName: string) => {
                                                                   if (file === "*") {
                                                                       if (orderedFiles.indexOf(fileName) === -1) {
                                                                           orderedFiles.push(fileName);
                                                                       }
                                                                   } else if ((
                                                                           new RegExp(file)
                                                                       ).test(fileName)) {
                                                                       orderedFiles.push(fileName);
                                                                   }
                                                               }
                                                           );
                                                           resolveOrder();
                                                       }
                                                   );
                                               }
                                           ).then(resolve).catch(reject);
                                       } else {
                                           orderedFiles = files;
                                           resolve();
                                       }
                                   }
                               ).then(
                                   () => {
                                       return Promise.each(
                                           orderedFiles, (file: string) => {
                                               filesPath.push(path + "/" + file);
                                           }
                                       );
                                   }
                               ).then(
                                   () => {
                                       return loader.requireFiles(
                                           filesPath, pieceName, (...args: Array<any>) => {
                                               args.unshift(category);
                                               return this.register.apply(this, args);
                                           }
                                       );
                                   }
                               );
                           }
                       );
    }

    /**
     * Return the object corresponding to pieceName and name
     * @param name
     * @param pieceName
     * @returns any
     */
    public get(name: string, pieceName: string|Array<string> = ["App", "Core"]): any {
        if (typeof pieceName === "string") {
            pieceName = [pieceName] as Array<string>;
        }

        let stringUtil: StringUtil;
        if (
            !this._cache.hasOwnProperty("Core") ||
            !this._cache.Core.hasOwnProperty("Util") ||
            !this._cache.Core.Util.hasOwnProperty("StringUtil")
        ) {
            stringUtil = new StringUtil();
        } else {
            stringUtil = this._cache.Core.Util.StringUtil;
        }

        let category = stringUtil.ucfirst(stringUtil.uncamelize(name).split("_").pop());

        for (let i = 0; i < pieceName.length; i++) {
            if (pieceName[i] in
                this._cache &&
                category in
                this._cache[pieceName[i]] &&
                name in
                this._cache[pieceName[i]][category]) {
                return this._cache[pieceName[i]][category][name];
            }
        }

        let pieceNames = Object.keys(this._cache);
        for (let i = 0; i < pieceNames.length; i++) {
            let currentPieceName = pieceNames[i];
            let categories       = Object.keys(this._cache[currentPieceName]);

            for (let z = 0; z < categories.length; z++) {
                let currentCategory = categories[z];

                if (
                    currentPieceName in this._cache &&
                    currentCategory in this._cache[currentPieceName] &&
                    name in this._cache[currentPieceName][currentCategory]
                ) {
                    return this._cache[currentPieceName][currentCategory][name];
                }
            }
        }

        return null;
    }

    /**
     * Execute actions after instantiation
     * @param afterInstantiate
     * @param obj
     * @returns {Promise<string[]>}
     * @private
     */
    private _executeAfterInstantiate(afterInstantiate: Array<string>|string, obj: any): Promise<Array<string>> {
        if (global._.isString(afterInstantiate)) {
            afterInstantiate = [afterInstantiate as string];
        }

        return Promise.each(
            afterInstantiate as Array<string>, (action: string) => {
                return obj[action]();
            }
        );
    }
}
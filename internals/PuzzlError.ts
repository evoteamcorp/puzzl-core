export class PuzzlError extends Error {

    public fatal: boolean = false;

    constructor(public message?: string, public code?: string, public datas?: any) {
        super(message);
        if (!message && !code) {
            this.message = "Error.Puzzl.UnknownError";
            this.code = "PU00001";
        }
    }

}
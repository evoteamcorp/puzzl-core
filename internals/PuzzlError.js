"use strict";
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var PuzzlError = (function (_super) {
    __extends(PuzzlError, _super);
    function PuzzlError(message, code, datas) {
        _super.call(this, message);
        this.message = message;
        this.code = code;
        this.datas = datas;
        this.fatal = false;
        if (!message && !code) {
            this.message = "Error.Puzzl.UnknownError";
            this.code = "PU00001";
        }
    }
    return PuzzlError;
}(Error));
exports.PuzzlError = PuzzlError;
//# sourceMappingURL=PuzzlError.js.map
import {FileUtil} from "../utils/FileUtil";
import {PathsUtil} from "../utils/PathsUtil";
import {ICore} from "../interfaces/ICore";
import {PuzzlError} from "../internals/PuzzlError";
import {PuzzlErrorStore} from "../manual_typings/built_in";
import {Instantiate} from "../decorators/common/Instantiate";
import {MetaObject} from "../decorators/common/MetaObject";
import {LoggerCore} from "./LoggerCore";
import {Injector} from "../decorators/injector/Injector";

@Injector()
@Instantiate(
    {
        afterInstantiate: [
            "parseAppPackage",
            "parsePuzzlPackage"
        ]
    }
)
@MetaObject()

export class PackageJsonCore implements ICore {

    // Store parsed packages
    private _packages: any = {};

    private _errorCodes: PuzzlErrorStore = {
        "PPJC0001": "Error.PackageJsonCore.checkPiece.NoPieceNameOrStoreOrDependencies"
    };

    constructor(private _pathsUtil: PathsUtil, private _fileUtil: FileUtil, private _logger: LoggerCore) {
    }

    /**
     * Parse application package.json
     * @returns {Promise}
     */
    public parseAppPackage(): Promise<void> {
        let promiseAppPackage = (resolve: (value?: any) => void, reject: (error: any) => void) => {

            let attemps   = ["./", "../", "../../", "../../../", "../../../../", "../../../../../"];
            let foundPath = null;

            Promise.each(
                attemps, (relPath: string): Promise<void> => {
                    let cPath = this._pathsUtil.getApp(relPath + "package.json");
                    return this._fileUtil.fileExist(cPath).then(
                        (result: boolean) => {
                            if (result) {
                                foundPath = cPath;
                            }
                        }
                    );
                }
            ).then(
                () => {
                    return this.parsePackage(foundPath, "App");
                }
            ).then(resolve).catch(reject);
        };

        return new Promise(promiseAppPackage);
    }

    /**
     * Parse Puzzl package.json
     * @returns {Promise}
     */
    public parsePuzzlPackage(): Promise<void> {
        return this.parsePackage(
            this._pathsUtil.getPuzzl("package.json"),
            "Puzzl"
        );
    }

    /**
     * Parse given package.json
     * @param file
     * @param piece
     * @returns {Promise<U>|Promise<TResult>}
     */
    public parsePackage(file: string, piece: string = "App"): Promise<void> {
        let resolveReadPackage = (packageJson: any) => {
            this._packages[piece] = packageJson;
        };

        return this.readPackage(file).then(resolveReadPackage);
    }

    /**
     * Return the app package json
     * @returns {*}
     */
    public getApp(): Object {
        return this._packages.App;
    }

    /**
     * Check if package is installed in given piece
     * @param name
     * @param pieceName
     * @returns {boolean}
     */
    public checkPieceIn(name: string, pieceName: string = "App"): boolean {
        return this.checkPiece(name, pieceName);
    }

    /**
     * Check if package is installed in given piece
     * @param name
     * @param pieceName
     * @returns {boolean}
     */
    public checkPiece(name: string, pieceName: string = "App"): boolean {
        if (name && pieceName in this._packages && "puzzlDeps" in this._packages[pieceName]) {
            let deps = this._packages[pieceName].puzzlDeps;

            return !!(
                name in deps
            );
        } else {
            this._logger.error(
                this._createError(
                    "PPJC0001", {
                        name     : name,
                        pieceName: pieceName
                    }
                )
            );
            return false;
        }
    }

    /**
     * Return packages matching pattern in given pieceName
     * @param pattern
     * @param pieceName
     * @returns {Array}
     */
    public getPiecesMatchingPattern(pattern: RegExp, pieceName: string = "App"): Array<string> {
        if (pieceName in this._packages && "puzzlDeps" in this._packages[pieceName]) {
            let deps = this._packages[pieceName].puzzlDeps;

            let pieces = [];

            for (let name in deps) {
                if (deps.hasOwnProperty(name) && pattern.test(name)) {
                    pieces.push(name);
                }
            }

            return pieces;
        } else {
            this._logger.error(
                this._createError(
                    "PPJC0001", {
                        name     : pattern.toString(),
                        pieceName: pieceName
                    }
                )
            );
            return [];
        }
    }

    /**
     * Return version of given package in given piece
     * @param name
     * @param pieceName
     * @returns {any}
     */
    public getPieceVersionIn(name: string, pieceName: string = "App"): string|boolean {
        return this.getPieceVersion(name, pieceName);
    }

    /**
     * Return version of given package in given piece
     * @param name
     * @param pieceName
     * @returns {any}
     */
    public getPieceVersion(name: string, pieceName: string = "App"): string|boolean {
        if (name && pieceName in this._packages && "puzzlDeps" in this._packages[pieceName]) {
            let deps = this._packages[pieceName].puzzlDeps;
            if (name in deps) {
                return deps[name];
            } else {
                return false;
            }
        } else {
            this._logger.error(
                this._createError(
                    "PPJC0001", {
                        name     : name,
                        pieceName: pieceName
                    }
                )
            );
            return false;
        }
    }

    /**
     * Check if application Puzzl version is different of core Puzzl version
     * @returns {boolean}
     */
    public checkPuzzlVersion(): boolean {
        let semver             = require("semver");
        let localPuzzlVersion  = semver.clean(this.getPieceVersion("puzzl-core"));
        let globalPuzzlVersion = semver.clean(this.getPuzzlVersion());

        return semver.gt(localPuzzlVersion, globalPuzzlVersion) || localPuzzlVersion === globalPuzzlVersion;
    }

    /**
     * Return Puzzl version
     * @returns {string|*}
     */
    public getPuzzlVersion(): string {
        return this._packages.Puzzl.version;
    }

    /**
     * Read given package.json
     * @param packagePath
     * @returns {Promise<U>|Thenable<U>|PromiseLike<TResult>|Promise<TResult>}
     */
    private readPackage(packagePath: string): Promise<any> {
        let resolveReadFile = function (content: Buffer): any {
            return JSON.parse(content.toString());
        };

        return this._fileUtil.readFile(packagePath)
                   .then(resolveReadFile);
    }

    /**
     * Create an error based on given code
     * @param code
     * @param datas
     * @returns {PuzzlError}
     * @private
     */
    private _createError(code: string, datas?: any): PuzzlError {
        if (code in this._errorCodes) {
            return new PuzzlError(this._errorCodes[code], code, datas);
        }

        return new PuzzlError();
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var StringUtil_1 = require("../utils/StringUtil");
var FileUtil_1 = require("../utils/FileUtil");
var Instantiate_1 = require("../decorators/common/Instantiate");
var MetaObject_1 = require("../decorators/common/MetaObject");
var Injector_1 = require("../decorators/injector/Injector");
var EventManager_1 = require("../managers/EventManager");
var LoaderCore = (function () {
    function LoaderCore(_stringUtil, _fileUtil, _eventManager) {
        this._stringUtil = _stringUtil;
        this._fileUtil = _fileUtil;
        this._eventManager = _eventManager;
        if (!_stringUtil.camelize) {
            this._stringUtil = new StringUtil_1.StringUtil();
        }
        if (!_fileUtil.dirExist) {
            this._fileUtil = new FileUtil_1.FileUtil();
        }
    }
    LoaderCore.prototype.require = function (filePath, pieceName, addFunction, fromEvent) {
        var _this = this;
        var promiseRequire = function (resolve, reject) {
            try {
                _this.cleanRequireCache(filePath);
                var obj = require(filePath);
                var firstKey = Object.keys(obj).shift();
                if (typeof obj !== "function" && typeof obj[firstKey] === "function") {
                    obj = obj[firstKey];
                }
                var className = _this._stringUtil.camelize(_this._fileUtil.getFileName(filePath));
                var result = addFunction(className, pieceName, obj);
                if (result instanceof Promise) {
                    result.then(resolve).catch(reject);
                }
                else {
                    resolve();
                }
            }
            catch (e) {
                reject(e);
            }
        };
        return new Promise(promiseRequire);
    };
    LoaderCore.prototype.requireNoPiece = function (filePath, addFunction, dontSearchFunction, fromEvent) {
        var _this = this;
        if (dontSearchFunction === void 0) { dontSearchFunction = false; }
        var promiseRequire = function (resolve, reject) {
            try {
                _this.cleanRequireCache(filePath);
                var obj = require(filePath);
                var firstKey = Object.keys(obj).shift();
                if (typeof obj !== "function" && typeof obj[firstKey] === "function" && !dontSearchFunction) {
                    obj = obj[firstKey];
                }
                var className = _this._stringUtil.ucfirst(_this._fileUtil.getFileName(filePath));
                var result = addFunction(className, obj);
                if (result instanceof Promise) {
                    result.then(resolve).catch(reject);
                }
                else {
                    resolve();
                }
            }
            catch (e) {
                reject(e);
            }
        };
        return new Promise(promiseRequire);
    };
    LoaderCore.prototype.requireFiles = function (files, pieceName, addFunction) {
        var _this = this;
        var requireFilesEach = function (file) {
            return _this.require(file, pieceName, addFunction);
        };
        return Promise.each(files, requireFilesEach);
    };
    LoaderCore.prototype.cleanRequireCache = function (filePath) {
        if (filePath in require.cache) {
            require.cache[filePath] = null;
        }
    };
    LoaderCore = __decorate([
        Injector_1.Injector(),
        Instantiate_1.Instantiate(),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [StringUtil_1.StringUtil, FileUtil_1.FileUtil, EventManager_1.EventManager])
    ], LoaderCore);
    return LoaderCore;
}());
exports.LoaderCore = LoaderCore;
//# sourceMappingURL=LoaderCore.js.map
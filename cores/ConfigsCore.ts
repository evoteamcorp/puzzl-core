import {FileUtil} from "../utils/FileUtil";
import {PathsUtil} from "../utils/PathsUtil";
import {ICore} from "../interfaces/ICore";
import {LoggerCore} from "./LoggerCore";
import {EventManager} from "../managers/EventManager";
import {Instantiate} from "../decorators/common/Instantiate";
import {MetaObject} from "../decorators/common/MetaObject";
import {Injector} from "../decorators/injector/Injector";
import {LoaderCore} from "./LoaderCore";
import {StringUtil} from "../utils/StringUtil";
import {Stats} from "fs";

@Injector()
@Instantiate(
    {
        afterInstantiate: [
            "loadPuzzlConfigs",
            "loadAppConfigs"
        ]
    }
)
@MetaObject()

/**
 * Class managing Configs
 */
export class ConfigsCore implements ICore {

    // Store of merged configs
    public _configs: any = {};

    public _reloadMode: boolean = false;

    constructor(
        private _fileUtil: FileUtil,
        private _pathsUtil: PathsUtil,
        private _loaderCore: LoaderCore,
        private _stringUtil: StringUtil,
        private _logger: LoggerCore,
        private _eventManager: EventManager
    ) {
        this._eventManager.on(
            "piece:FileWatcher:initialized", () => {
                Puzzl.getInjector().get("WatchManager").addWatchPath(
                    {
                        callback: (eventType: string, file?: string, stat?: Stats): void => {
                            this.loadAppConfigs();
                        },
                        eventType: ["add", "change"],
                        path: this._pathsUtil.getApp("configs")
                    }
                );

                Puzzl.getInjector().get("WatchManager").addWatchPath(
                    {
                        callback: (eventType: string, file?: string, stat?: Stats): void => {
                            this.loadAppConfigs();
                        },
                        eventType: ["add", "change"],
                        path: this._pathsUtil.getPuzzl("configs")
                    }
                );
            }
        );
    }

    /**
     * Load application configuration
     * @return {Promise<Array<string>>}
     */
    public loadAppConfigs(): Promise<Array<string>> {
        let resolveGetFilesInDir = (files: Array<string>) => {
            let promiseEachConfigs = (file: string) => {
                return this._loaderCore.requireNoPiece(
                    this._pathsUtil.getApp("configs/" + file),
                    (...args: Array<any>) => {
                        return this.registerConfig.apply(this, args);
                    },
                    true
                );
            };

            return Promise.each(files, promiseEachConfigs);
        };

        return this._fileUtil.dirExist(this._pathsUtil.getApp("configs")).then(
            (result: boolean) => {
                return new Promise(
                    (resolve: (value: any) => Array<string>, reject: (error: any) => void) => {
                        if (result) {
                            this._fileUtil.getFilesInDir(this._pathsUtil.getApp("configs"))
                                .then(resolve).catch(reject);
                        } else {
                            resolve([]);
                        }
                    }
                ).then(resolveGetFilesInDir);
            }
        );
    }

    /**
     * Reload application configurations
     * @returns {Promise<void>}
     */
    public reloadAppConfigs(): Promise<void> {
        this._reloadMode = true;
        return this.loadAppConfigs().then(
            () => {
                this._reloadMode = false;
            }
        );
    }

    /**
     * Load Puzzl base configuration
     * @returns {Promise<Array<string>>}
     */
    public loadPuzzlConfigs(): Promise<Array<string>> {
        let resolveGetFilesInDir = (files: Array<string>) => {
            let promiseEachConfigs = (file: string) => {
                return this._loaderCore.requireNoPiece(
                    this._pathsUtil.getPuzzl("configs/" + file),
                    (...args: Array<any>) => {
                        this.registerConfig.apply(this, args);
                    },
                    true
                );
            };

            return Promise.each(files, promiseEachConfigs);
        };

        return new Promise(
            (resolve: (value?: any) => void, reject: (error: any) => void) => {
                return this._fileUtil.getFilesInDir(
                    this._pathsUtil.getPuzzl("configs")
                ).then(resolveGetFilesInDir).then(resolve).catch(
                    (err: any) => {
                        if (err.code === "PFU0001") {
                            if (this._logger.warn) {
                                this._logger.warn("Warning.ConfigsCore.MissingPuzzlConfigFiles");
                            }
                            resolve();
                        } else {
                            reject(err);
                        }
                    }
                );
            }
        );

    }

    /**
     * Register a piece config
     * @param pieceName
     * @param configFolder
     * @returns {Promise<any>}
     */
    public loadPieceConfigs(pieceName: string, configFolder: string = "configs", fromEvent: boolean = false): Promise<any> {
        if (!fromEvent) {
            this._eventManager.on("piece:FileWatcher:initialized", () => {
                Puzzl.getInjector().get("WatchManager").addWatchPath(
                    {
                        callback: (eventType: string, file?: string, stat?: Stats): void => {
                            this.loadPieceConfigs(pieceName, configFolder, true);
                        },
                        eventType: ["add", "change"],
                        path: this._pathsUtil.getPiecePath(pieceName, configFolder)
                    }
                );
            });
        }

        let resolveGetFilesInDir = (files: Array<string>) => {
            let promiseEachConfigs = (file: string) => {
                return this._loaderCore.requireNoPiece(
                    this._pathsUtil.getPiecePath(pieceName, configFolder + "/" + file),
                    (...args: Array<any>) => {
                        this.registerConfig.apply(this, args);
                    },
                    true
                );
            };

            return Promise.each(files, promiseEachConfigs);
        };

        let promiseLoadPieceConfigs = (resolve: (value: any) => void, reject: (error: any) => void) => {
            let path = this._pathsUtil.getPiecePath(pieceName, configFolder);
            this._fileUtil.dirExist(path)
                .then(
                    (result: boolean) => {
                        return new Promise(
                            (resolveInject: (value: any) => Array<string>, rejectInject: (error: any) => void) => {
                                if (result) {
                                    this._fileUtil.getFilesInDir(
                                        path
                                    ).then(resolveInject).catch(rejectInject);
                                } else {
                                    if (this._logger.warn) {
                                        this._logger.warn("Warning.ConfigsCore.MissingPieceConfigPath", path);
                                    }
                                    resolve([]);
                                }
                            }
                        );
                    }
                ).then(resolveGetFilesInDir).then(resolve).catch(reject);
        };

        return new Promise(promiseLoadPieceConfigs);

    }

    /**
     * Register application configuration
     * @param name
     * @param object
     * @returns {Promise<void>}
     */
    public registerConfig(name: string, object: any): Promise<void> {
        let promiseRegisterConfig = (resolve: (value?: any) => void, reject: (error: any) => void) => {
            try {
                if (name in this._configs) {
                    this._configs[name] = global._.merge(this._configs[name], object);
                    if (this._eventManager.emit) {
                        this._eventManager.emit("configs:" + name + ":updated", object);
                    }
                } else {
                    this._configs[name] = object;
                }
                resolve();
            } catch (e) {
                reject(e);
            }
        };

        return new Promise(promiseRegisterConfig);
    }

    /**
     * Return configs matching name
     * @param name
     * @returns {any}
     */
    public get(name: string): any {
        name = this._stringUtil.ucfirst(name);

        if (name in this._configs) {
            return this._configs[name];
        } else {
            return null;
        }
    }
}

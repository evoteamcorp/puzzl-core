"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var ConfigsCore_1 = require("./ConfigsCore");
var Instantiate_1 = require("../decorators/common/Instantiate");
var MetaObject_1 = require("../decorators/common/MetaObject");
var Injector_1 = require("../decorators/injector/Injector");
require("better-log").install({ depth: 10 });
var LoggerCore = (function () {
    function LoggerCore(_configsCore) {
        this._configsCore = _configsCore;
        this._logger = console;
    }
    LoggerCore.prototype.log = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        args.unshift("\u001B[37m\u001B[1m[LOG]:\u001B[0m");
        this._addTime(args);
        this._checkLogger();
        this._logger.log.apply(console, args);
    };
    LoggerCore.prototype.info = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        args.unshift("\u001B[36m\u001B[1m[INFO]:\u001B[0m");
        this._addTime(args);
        this._checkLogger();
        this._logger.info.apply(console, args);
    };
    LoggerCore.prototype.warn = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        args.unshift("\u001B[33m\u001B[1m[WARN]:\u001B[0m");
        this._addTime(args);
        this._checkLogger();
        this._logger.warn.apply(console, args);
    };
    LoggerCore.prototype.error = function (err) {
        var args = [];
        var hasDetails = false;
        this._addTime(args);
        if (err && err.fatal) {
            args.push("\u001B[31m\u001B[1m[FATAL]:\u001B[0m");
            args.push(err.stack);
        }
        else if (err) {
            args.push("\u001B[31m\u001B[1m[ERR " + err.code + "]:\u001B[0m");
            args.push(err.message);
            if (err.datas) {
                args.push(", \u001B[36mDatas:\u001B[0m");
                args.push(err.datas);
            }
            if (err.stack) {
                args.push("Details:");
                hasDetails = true;
            }
        }
        this._checkLogger();
        this._logger.error.apply(console, args);
        if (hasDetails) {
            this._logger.error(err.stack);
        }
        if (err && err.fatal) {
            process.exit(1);
        }
    };
    LoggerCore.prototype.debug = function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i - 0] = arguments[_i];
        }
        if (this._configsCore.get && this._configsCore.get("CoreConfig") && this._configsCore.get("CoreConfig").debug) {
            args.unshift("\u001B[100m\u001B[1m[DEBUG]:\u001B[0m");
            this._addTime(args);
            this._checkLogger();
            this._logger.log.apply(console, args);
        }
    };
    LoggerCore.prototype._addTime = function (args) {
        args.unshift("\u001B[35m" +
            ((new Date()).toISOString()) +
            ":\u001B[0m");
    };
    LoggerCore.prototype._checkLogger = function () {
        if (this._configsCore.get) {
            this._logger = this._configsCore.get("CoreConfig").logger;
        }
    };
    LoggerCore = __decorate([
        Injector_1.Injector(),
        Instantiate_1.Instantiate(),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [ConfigsCore_1.ConfigsCore])
    ], LoggerCore);
    return LoggerCore;
}());
exports.LoggerCore = LoggerCore;
//# sourceMappingURL=LoggerCore.js.map
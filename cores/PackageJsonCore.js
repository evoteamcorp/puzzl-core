"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var FileUtil_1 = require("../utils/FileUtil");
var PathsUtil_1 = require("../utils/PathsUtil");
var PuzzlError_1 = require("../internals/PuzzlError");
var Instantiate_1 = require("../decorators/common/Instantiate");
var MetaObject_1 = require("../decorators/common/MetaObject");
var LoggerCore_1 = require("./LoggerCore");
var Injector_1 = require("../decorators/injector/Injector");
var PackageJsonCore = (function () {
    function PackageJsonCore(_pathsUtil, _fileUtil, _logger) {
        this._pathsUtil = _pathsUtil;
        this._fileUtil = _fileUtil;
        this._logger = _logger;
        this._packages = {};
        this._errorCodes = {
            "PPJC0001": "Error.PackageJsonCore.checkPiece.NoPieceNameOrStoreOrDependencies"
        };
    }
    PackageJsonCore.prototype.parseAppPackage = function () {
        var _this = this;
        var promiseAppPackage = function (resolve, reject) {
            var attemps = ["./", "../", "../../", "../../../", "../../../../", "../../../../../"];
            var foundPath = null;
            Promise.each(attemps, function (relPath) {
                var cPath = _this._pathsUtil.getApp(relPath + "package.json");
                return _this._fileUtil.fileExist(cPath).then(function (result) {
                    if (result) {
                        foundPath = cPath;
                    }
                });
            }).then(function () {
                return _this.parsePackage(foundPath, "App");
            }).then(resolve).catch(reject);
        };
        return new Promise(promiseAppPackage);
    };
    PackageJsonCore.prototype.parsePuzzlPackage = function () {
        return this.parsePackage(this._pathsUtil.getPuzzl("package.json"), "Puzzl");
    };
    PackageJsonCore.prototype.parsePackage = function (file, piece) {
        var _this = this;
        if (piece === void 0) { piece = "App"; }
        var resolveReadPackage = function (packageJson) {
            _this._packages[piece] = packageJson;
        };
        return this.readPackage(file).then(resolveReadPackage);
    };
    PackageJsonCore.prototype.getApp = function () {
        return this._packages.App;
    };
    PackageJsonCore.prototype.checkPieceIn = function (name, pieceName) {
        if (pieceName === void 0) { pieceName = "App"; }
        return this.checkPiece(name, pieceName);
    };
    PackageJsonCore.prototype.checkPiece = function (name, pieceName) {
        if (pieceName === void 0) { pieceName = "App"; }
        if (name && pieceName in this._packages && "puzzlDeps" in this._packages[pieceName]) {
            var deps = this._packages[pieceName].puzzlDeps;
            return !!(name in deps);
        }
        else {
            this._logger.error(this._createError("PPJC0001", {
                name: name,
                pieceName: pieceName
            }));
            return false;
        }
    };
    PackageJsonCore.prototype.getPiecesMatchingPattern = function (pattern, pieceName) {
        if (pieceName === void 0) { pieceName = "App"; }
        if (pieceName in this._packages && "puzzlDeps" in this._packages[pieceName]) {
            var deps = this._packages[pieceName].puzzlDeps;
            var pieces = [];
            for (var name_1 in deps) {
                if (deps.hasOwnProperty(name_1) && pattern.test(name_1)) {
                    pieces.push(name_1);
                }
            }
            return pieces;
        }
        else {
            this._logger.error(this._createError("PPJC0001", {
                name: pattern.toString(),
                pieceName: pieceName
            }));
            return [];
        }
    };
    PackageJsonCore.prototype.getPieceVersionIn = function (name, pieceName) {
        if (pieceName === void 0) { pieceName = "App"; }
        return this.getPieceVersion(name, pieceName);
    };
    PackageJsonCore.prototype.getPieceVersion = function (name, pieceName) {
        if (pieceName === void 0) { pieceName = "App"; }
        if (name && pieceName in this._packages && "puzzlDeps" in this._packages[pieceName]) {
            var deps = this._packages[pieceName].puzzlDeps;
            if (name in deps) {
                return deps[name];
            }
            else {
                return false;
            }
        }
        else {
            this._logger.error(this._createError("PPJC0001", {
                name: name,
                pieceName: pieceName
            }));
            return false;
        }
    };
    PackageJsonCore.prototype.checkPuzzlVersion = function () {
        var semver = require("semver");
        var localPuzzlVersion = semver.clean(this.getPieceVersion("puzzl-core"));
        var globalPuzzlVersion = semver.clean(this.getPuzzlVersion());
        return semver.gt(localPuzzlVersion, globalPuzzlVersion) || localPuzzlVersion === globalPuzzlVersion;
    };
    PackageJsonCore.prototype.getPuzzlVersion = function () {
        return this._packages.Puzzl.version;
    };
    PackageJsonCore.prototype.readPackage = function (packagePath) {
        var resolveReadFile = function (content) {
            return JSON.parse(content.toString());
        };
        return this._fileUtil.readFile(packagePath)
            .then(resolveReadFile);
    };
    PackageJsonCore.prototype._createError = function (code, datas) {
        if (code in this._errorCodes) {
            return new PuzzlError_1.PuzzlError(this._errorCodes[code], code, datas);
        }
        return new PuzzlError_1.PuzzlError();
    };
    PackageJsonCore = __decorate([
        Injector_1.Injector(),
        Instantiate_1.Instantiate({
            afterInstantiate: [
                "parseAppPackage",
                "parsePuzzlPackage"
            ]
        }),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [PathsUtil_1.PathsUtil, FileUtil_1.FileUtil, LoggerCore_1.LoggerCore])
    ], PackageJsonCore);
    return PackageJsonCore;
}());
exports.PackageJsonCore = PackageJsonCore;
//# sourceMappingURL=PackageJsonCore.js.map
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var FileUtil_1 = require("../utils/FileUtil");
var PathsUtil_1 = require("../utils/PathsUtil");
var LoggerCore_1 = require("./LoggerCore");
var EventManager_1 = require("../managers/EventManager");
var Instantiate_1 = require("../decorators/common/Instantiate");
var MetaObject_1 = require("../decorators/common/MetaObject");
var Injector_1 = require("../decorators/injector/Injector");
var LoaderCore_1 = require("./LoaderCore");
var StringUtil_1 = require("../utils/StringUtil");
var ConfigsCore = (function () {
    function ConfigsCore(_fileUtil, _pathsUtil, _loaderCore, _stringUtil, _logger, _eventManager) {
        var _this = this;
        this._fileUtil = _fileUtil;
        this._pathsUtil = _pathsUtil;
        this._loaderCore = _loaderCore;
        this._stringUtil = _stringUtil;
        this._logger = _logger;
        this._eventManager = _eventManager;
        this._configs = {};
        this._reloadMode = false;
        this._eventManager.on("piece:FileWatcher:initialized", function () {
            Puzzl.getInjector().get("WatchManager").addWatchPath({
                callback: function (eventType, file, stat) {
                    _this.loadAppConfigs();
                },
                eventType: ["add", "change"],
                path: _this._pathsUtil.getApp("configs")
            });
            Puzzl.getInjector().get("WatchManager").addWatchPath({
                callback: function (eventType, file, stat) {
                    _this.loadAppConfigs();
                },
                eventType: ["add", "change"],
                path: _this._pathsUtil.getPuzzl("configs")
            });
        });
    }
    ConfigsCore.prototype.loadAppConfigs = function () {
        var _this = this;
        var resolveGetFilesInDir = function (files) {
            var promiseEachConfigs = function (file) {
                return _this._loaderCore.requireNoPiece(_this._pathsUtil.getApp("configs/" + file), function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i - 0] = arguments[_i];
                    }
                    return _this.registerConfig.apply(_this, args);
                }, true);
            };
            return Promise.each(files, promiseEachConfigs);
        };
        return this._fileUtil.dirExist(this._pathsUtil.getApp("configs")).then(function (result) {
            return new Promise(function (resolve, reject) {
                if (result) {
                    _this._fileUtil.getFilesInDir(_this._pathsUtil.getApp("configs"))
                        .then(resolve).catch(reject);
                }
                else {
                    resolve([]);
                }
            }).then(resolveGetFilesInDir);
        });
    };
    ConfigsCore.prototype.reloadAppConfigs = function () {
        var _this = this;
        this._reloadMode = true;
        return this.loadAppConfigs().then(function () {
            _this._reloadMode = false;
        });
    };
    ConfigsCore.prototype.loadPuzzlConfigs = function () {
        var _this = this;
        var resolveGetFilesInDir = function (files) {
            var promiseEachConfigs = function (file) {
                return _this._loaderCore.requireNoPiece(_this._pathsUtil.getPuzzl("configs/" + file), function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i - 0] = arguments[_i];
                    }
                    _this.registerConfig.apply(_this, args);
                }, true);
            };
            return Promise.each(files, promiseEachConfigs);
        };
        return new Promise(function (resolve, reject) {
            return _this._fileUtil.getFilesInDir(_this._pathsUtil.getPuzzl("configs")).then(resolveGetFilesInDir).then(resolve).catch(function (err) {
                if (err.code === "PFU0001") {
                    if (_this._logger.warn) {
                        _this._logger.warn("Warning.ConfigsCore.MissingPuzzlConfigFiles");
                    }
                    resolve();
                }
                else {
                    reject(err);
                }
            });
        });
    };
    ConfigsCore.prototype.loadPieceConfigs = function (pieceName, configFolder, fromEvent) {
        var _this = this;
        if (configFolder === void 0) { configFolder = "configs"; }
        if (fromEvent === void 0) { fromEvent = false; }
        if (!fromEvent) {
            this._eventManager.on("piece:FileWatcher:initialized", function () {
                Puzzl.getInjector().get("WatchManager").addWatchPath({
                    callback: function (eventType, file, stat) {
                        _this.loadPieceConfigs(pieceName, configFolder, true);
                    },
                    eventType: ["add", "change"],
                    path: _this._pathsUtil.getPiecePath(pieceName, configFolder)
                });
            });
        }
        var resolveGetFilesInDir = function (files) {
            var promiseEachConfigs = function (file) {
                return _this._loaderCore.requireNoPiece(_this._pathsUtil.getPiecePath(pieceName, configFolder + "/" + file), function () {
                    var args = [];
                    for (var _i = 0; _i < arguments.length; _i++) {
                        args[_i - 0] = arguments[_i];
                    }
                    _this.registerConfig.apply(_this, args);
                }, true);
            };
            return Promise.each(files, promiseEachConfigs);
        };
        var promiseLoadPieceConfigs = function (resolve, reject) {
            var path = _this._pathsUtil.getPiecePath(pieceName, configFolder);
            _this._fileUtil.dirExist(path)
                .then(function (result) {
                return new Promise(function (resolveInject, rejectInject) {
                    if (result) {
                        _this._fileUtil.getFilesInDir(path).then(resolveInject).catch(rejectInject);
                    }
                    else {
                        if (_this._logger.warn) {
                            _this._logger.warn("Warning.ConfigsCore.MissingPieceConfigPath", path);
                        }
                        resolve([]);
                    }
                });
            }).then(resolveGetFilesInDir).then(resolve).catch(reject);
        };
        return new Promise(promiseLoadPieceConfigs);
    };
    ConfigsCore.prototype.registerConfig = function (name, object) {
        var _this = this;
        var promiseRegisterConfig = function (resolve, reject) {
            try {
                if (name in _this._configs) {
                    _this._configs[name] = global._.merge(_this._configs[name], object);
                    if (_this._eventManager.emit) {
                        _this._eventManager.emit("configs:" + name + ":updated", object);
                    }
                }
                else {
                    _this._configs[name] = object;
                }
                resolve();
            }
            catch (e) {
                reject(e);
            }
        };
        return new Promise(promiseRegisterConfig);
    };
    ConfigsCore.prototype.get = function (name) {
        name = this._stringUtil.ucfirst(name);
        if (name in this._configs) {
            return this._configs[name];
        }
        else {
            return null;
        }
    };
    ConfigsCore = __decorate([
        Injector_1.Injector(),
        Instantiate_1.Instantiate({
            afterInstantiate: [
                "loadPuzzlConfigs",
                "loadAppConfigs"
            ]
        }),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [FileUtil_1.FileUtil, PathsUtil_1.PathsUtil, LoaderCore_1.LoaderCore, StringUtil_1.StringUtil, LoggerCore_1.LoggerCore, EventManager_1.EventManager])
    ], ConfigsCore);
    return ConfigsCore;
}());
exports.ConfigsCore = ConfigsCore;
//# sourceMappingURL=ConfigsCore.js.map
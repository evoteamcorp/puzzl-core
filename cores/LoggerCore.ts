import {ICore} from "../interfaces/ICore";
import {ConfigsCore} from "./ConfigsCore";
import {Instantiate} from "../decorators/common/Instantiate";
import {MetaObject} from "../decorators/common/MetaObject";
import {Injector} from "../decorators/injector/Injector";
require("better-log").install({depth: 10});

@Injector()
@Instantiate()
@MetaObject()

/**
 * Class managing Logs
 */
export class LoggerCore implements ICore {

    private _logger: any = console;

    constructor(private _configsCore: ConfigsCore) {
    }

    /**
     * Log a message
     * @param args
     */
    public log(...args: Array<any>): void {
        args.unshift("\u001B[37m\u001B[1m[LOG]:\u001B[0m");
        this._addTime(args);
        this._checkLogger();
        this._logger.log.apply(console, args);
    }

    /**
     * Log an info
     * @param args
     */
    public info(...args: Array<any>): void {
        args.unshift("\u001B[36m\u001B[1m[INFO]:\u001B[0m");
        this._addTime(args);
        this._checkLogger();
        this._logger.info.apply(console, args);
    }

    /**
     * Log a warning
     * @param args
     */
    public warn(...args: Array<any>): void {
        args.unshift("\u001B[33m\u001B[1m[WARN]:\u001B[0m");
        this._addTime(args);
        this._checkLogger();
        this._logger.warn.apply(console, args);
    }

    /**
     * Log an error and kill process if fatal
     * @param err
     */
    public error(err: any): void {
        let args: Array<string> = [];
        let hasDetails          = false;
        this._addTime(args);
        if (err && err.fatal) {
            args.push("\u001B[31m\u001B[1m[FATAL]:\u001B[0m");
            args.push(err.stack);
        } else if (err) {
            args.push("\u001B[31m\u001B[1m[ERR " + err.code + "]:\u001B[0m");
            args.push(err.message);
            if (err.datas) {
                args.push(", \u001B[36mDatas:\u001B[0m");
                args.push(err.datas);
            }
            if (err.stack) {
                args.push("Details:");
                hasDetails = true;
            }
        }
        this._checkLogger();
        this._logger.error.apply(console, args);
        if (hasDetails) {
            this._logger.error(err.stack);
        }
        if (err && err.fatal) {
            process.exit(1);
        }
    }

    /**
     * Log a debug
     * @param args
     */
    public debug(...args: Array<any>): void {
        if (this._configsCore.get && this._configsCore.get("CoreConfig") && this._configsCore.get("CoreConfig").debug) {
            args.unshift("\u001B[100m\u001B[1m[DEBUG]:\u001B[0m");
            this._addTime(args);
            this._checkLogger();
            this._logger.log.apply(console, args);
        }
    }

    /**
     * Add the time into logs
     * @param args
     * @private
     */
    private _addTime(args: Array<any>): void {
        args.unshift(
            "\u001B[35m" +
            (
                (
                    new Date()
                ).toISOString()
            ) +
            ":\u001B[0m"
        );
    }

    /**
     * Update the logger backend
     * @private
     */
    private _checkLogger(): void {
        if (this._configsCore.get) {
            this._logger = this._configsCore.get("CoreConfig").logger;
        }
    }
}
/**
 * Class Managing require files
 */
import {StringUtil} from "../utils/StringUtil";
import {FileUtil} from "../utils/FileUtil";
import {Instantiate} from "../decorators/common/Instantiate";
import {MetaObject} from "../decorators/common/MetaObject";
import {ICore} from "../interfaces/ICore";
import {Injector} from "../decorators/injector/Injector";
import {EventManager} from "../managers/EventManager";

@Injector()
@Instantiate()
@MetaObject()

export class LoaderCore implements ICore {

    constructor(private _stringUtil?: StringUtil, private _fileUtil?: FileUtil, private _eventManager?: EventManager) {
        if (!_stringUtil.camelize) {
            this._stringUtil = new StringUtil();
        }

        if (!_fileUtil.dirExist) {
            this._fileUtil = new FileUtil();
        }
    }

    /**
     * Load a file
     * @param filePath
     * @param pieceName
     * @param addFunction
     * @returns {any}
     */
    public require(
        filePath: string,
        pieceName: string,
        addFunction: (name: string, pieceName: string, object: any) => any,
        fromEvent?: boolean
    ): Promise<void> {
        let promiseRequire = (resolve: (value?: any) => void, reject: (error: any) => void): void => {
            try {
                this.cleanRequireCache(filePath);
                let obj = require(filePath);

                let firstKey = Object.keys(obj).shift();
                if (typeof obj !== "function" && typeof obj[firstKey] === "function") {
                    obj = obj[firstKey];
                }

                let className = this._stringUtil.camelize(this._fileUtil.getFileName(filePath));

                let result = addFunction(className, pieceName, obj);

                if (result instanceof Promise) {
                    result.then(resolve).catch(reject);
                } else {
                    resolve();
                }
            } catch (e) {
                reject(e);
            }
        };
        return new Promise(promiseRequire);
    }

    /**
     * Load a file without piece
     * @param filePath
     * @param addFunction
     * @param dontSearchFunction
     * @returns {Promise<void>}
     */
    public requireNoPiece(
        filePath: string,
        addFunction: (name: string, object: any) => any,
        dontSearchFunction: boolean = false,
        fromEvent?: boolean
    ): Promise<void> {
        let promiseRequire = (resolve: (value?: any) => void, reject: (error: any) => void): void => {
            try {
                this.cleanRequireCache(filePath);
                let obj = require(filePath);

                let firstKey = Object.keys(obj).shift();
                if (typeof obj !== "function" && typeof obj[firstKey] === "function" && !dontSearchFunction) {
                    obj = obj[firstKey];
                }

                let className = this._stringUtil.ucfirst(this._fileUtil.getFileName(filePath));

                let result = addFunction(className, obj);

                if (result instanceof Promise) {
                    result.then(resolve).catch(reject);
                } else {
                    resolve();
                }
            } catch (e) {
                reject(e);
            }
        };
        return new Promise(promiseRequire);
    }

    /**
     * Load files
     * @param files
     * @param pieceName
     * @param addFunction
     * @returns {any}
     */
    public requireFiles(
        files: Array<string>,
        pieceName: string,
        addFunction: (name: string, pieceName: string, object: any) => any
    ): Promise<Array<string>> {
        let requireFilesEach = (file: string) => {
            return this.require(file, pieceName, addFunction);
        };
        return Promise.each(files, requireFilesEach);
    }

    /**
     * Remove an entry from require cache
     * @param filePath
     */
    public cleanRequireCache(filePath: string): void {
        if (filePath in require.cache) {
            require.cache[filePath] = null;
        }
    }
}
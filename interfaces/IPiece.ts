export interface IPiece {
    promise: Promise<void>;
    initialize: (initialized: (value?: any) => void, error: (error?: any) => void) => void;
    getPath: () => string;
}

export interface CPiece {
    new (): IPiece;
}
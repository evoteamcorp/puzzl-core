# Puzzl-Core #

Puzzl-Core est le coeur du framework Puzzl il contient plusieurs utilitaire permettant de facilement créer / charger une pièce de Puzzl.

Puzzl est un framework modulaire qui permet de facilement ajouter un module à votre application sans configuration compliqué ou gestion des dépendances rébarbatives.

Etant donné le nom du framework qui est Puzzl les modules installables sont appelés des pièces.

### Informations de la pièce ###

* **Nom**: Puzzl-Core
* **Version**: 0.0.1
* **Date de release**: N/A

### Pré-requis ###

* **Versions de NodeJS / iojs**
* * **NodeJS**: >= 6
* * **iojs**: N/A
* **Dépendances**
* * **better-log**: ^1.3.3
* * **bluebird**: ^3.4.0
* * **lodash**: ^4.12.0
* * **reflect**-metadata: ^0.1.3
* * **semver**: ^5.1.0
* * **source-map-support**: ^0.4.0

### Comment contribuer ###

* Ecrire des tests unitaires, car je n'ai aucune idée de comment m'y prendre T_T
* Ne pas hésiter à proposer des améliorations
* Ouvrir un ticket en cas de bug

### Qui est derrière Puzzl et comment me contacter ###

* Anthony Briand (acidspike) <ace130210@gmail.com>
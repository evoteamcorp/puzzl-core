"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var path = require("path");
var fs = require("fs");
var PuzzlError_1 = require("../internals/PuzzlError");
var Instantiate_1 = require("../decorators/common/Instantiate");
var MetaObject_1 = require("../decorators/common/MetaObject");
var FileUtil = (function () {
    function FileUtil() {
        this._excludedFilesPatterns = ["\\.map$", "\\.d\\.ts$", "\\.ts$"];
        this._errorCodes = {
            "PFU0001": "Error.FileUtil.getFilesInDir.FolderNotExists",
            "PFU0002": "Error.FileUtil.getFilesInDir.FileNotExists"
        };
    }
    FileUtil.prototype.getFileName = function (filePath, withExtension) {
        if (withExtension === void 0) { withExtension = false; }
        var extName = (!withExtension) ? path.extname(filePath) : null;
        return path.basename(filePath, extName);
    };
    FileUtil.prototype.dirExist = function (path) {
        var promiseDirExist = function (resolve, reject) {
            var callbackStat = function (err, stat) {
                if (err && err.code !== "ENOENT") {
                    reject(err);
                }
                else if (stat) {
                    resolve(stat.isDirectory());
                }
                else {
                    resolve(false);
                }
            };
            fs.stat(path, callbackStat);
        };
        return new Promise(promiseDirExist);
    };
    FileUtil.prototype.fileExist = function (path) {
        var promiseFileExist = function (resolve, reject) {
            var callbackStatFile = function (err, stat) {
                if (err && err.code !== "ENOENT") {
                    reject(err);
                }
                else if (stat) {
                    resolve(stat.isFile());
                }
                else {
                    resolve(false);
                }
            };
            fs.stat(path, callbackStatFile);
        };
        return new Promise(promiseFileExist);
    };
    FileUtil.prototype.readFile = function (path) {
        var _this = this;
        var resolveFileExist = function (exist) {
            var promiseReadFile = function (resolve, reject) {
                if (exist) {
                    var callbackReadFile = function (err, data) {
                        if (err) {
                            reject(err);
                        }
                        else {
                            resolve(data);
                        }
                    };
                    fs.readFile(path, callbackReadFile);
                }
                else {
                    var err = _this._createError("PFU0002", { path: path });
                    reject(err);
                }
            };
            return new Promise(promiseReadFile);
        };
        return this.fileExist(path).then(resolveFileExist);
    };
    FileUtil.prototype.getFilesInDir = function (path) {
        var _this = this;
        var resolveDirExist = function (exist) {
            var promiseReadDir = function (resolve, reject) {
                if (exist) {
                    var callbackReadDir = function (err, files) {
                        if (err) {
                            reject(err);
                        }
                        var fFiles = [];
                        files.forEach(function (file) {
                            var matchPattern = false;
                            _this._excludedFilesPatterns.forEach(function (pattern) {
                                if ((new RegExp(pattern)).test(file) && !matchPattern) {
                                    matchPattern = true;
                                }
                            });
                            if (!matchPattern) {
                                fFiles.push(file);
                            }
                        });
                        resolve(fFiles);
                    };
                    fs.readdir(path, callbackReadDir);
                }
                else {
                    var err = _this._createError("PFU0001", { path: path });
                    reject(err);
                }
            };
            return new Promise(promiseReadDir);
        };
        return this.dirExist(path)
            .then(resolveDirExist);
    };
    FileUtil.prototype._createError = function (code, datas) {
        if (code in this._errorCodes) {
            return new PuzzlError_1.PuzzlError(this._errorCodes[code], code, datas);
        }
        return new PuzzlError_1.PuzzlError();
    };
    FileUtil = __decorate([
        Instantiate_1.Instantiate(),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [])
    ], FileUtil);
    return FileUtil;
}());
exports.FileUtil = FileUtil;
//# sourceMappingURL=FileUtil.js.map
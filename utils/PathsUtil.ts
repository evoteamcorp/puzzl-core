import path = require("path");
import {IUtil} from "../interfaces/IUtil";
import {PathsDictionary} from "../manual_typings/built_in";
import {Instantiate} from "../decorators/common/Instantiate";
import {MetaObject} from "../decorators/common/MetaObject";

@Instantiate()
@MetaObject()

/**
 * Class Managing paths
 */
export class PathsUtil implements IUtil {

    // Application Path
    private _appPath: string = path.dirname(require.main.filename);

    // Puzzl Framework Path
    private _puzzlPath: string = path.resolve(__dirname + "/../");

    // Pieces paths
    private _piecesPath: PathsDictionary = {};

    /**
     * Return Application path
     * @param extraPath
     * @returns {string}
     */
    public getApp(extraPath?: string): string {
        let p = this._appPath;
        if (extraPath != null) {
            p += "/" + extraPath;
        }

        return p;
    }

    /**
     * Return Puzzl path
     * @param extraPath
     * @returns {string}
     */
    public getPuzzl(extraPath?: string): string {
        let p = this._puzzlPath;
        if (extraPath != null) {
            p += "/" + extraPath;
        }

        return p;
    }

    /**
     * Return path to given util
     * @param name
     * @returns {string}
     */
    public getPuzzlUtils(name?: string): string {
        return this.getPuzzl("utils") +
               (
                   (
                       name
                   ) ? "/" + name : ""
               );
    }

    /**
     * Return path to given core
     * @param name
     * @returns {string}
     */
    public getPuzzlCores(name?: string): string {
        return this.getPuzzl("cores") +
               (
                   (
                       name
                   ) ? "/" + name : ""
               );
    }

    /**
     * Add a piece path
     * @param name
     * @param path
     */
    public addPiecePath(name: string, path: string): void {
        this._piecesPath[name] = path;
    }

    /**
     * Return piece path
     * @param name
     * @param extraPath
     */
    public getPiecePath(name: string, extraPath?: string): string {
        let p = this._piecesPath[name];
        if (extraPath != null) {
            p += "/" + extraPath;
        }

        return p;
    }
}
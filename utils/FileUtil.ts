import path = require("path");
import fs = require("fs");
import {IUtil} from "../interfaces/IUtil";
import {Stats} from "fs";
import {PuzzlError} from "../internals/PuzzlError";
import {PuzzlErrorStore} from "../manual_typings/built_in";
import {Instantiate} from "../decorators/common/Instantiate";
import {MetaObject} from "../decorators/common/MetaObject";

@Instantiate()
@MetaObject()

/**
 * Class managing files
 */
export class FileUtil implements IUtil {

    /**
     * Patterns for excluded files
     * @type {string[]}
     * @private
     */
    private _excludedFilesPatterns: Array<string> = ["\\.map$", "\\.d\\.ts$", "\\.ts$"];

    /**
     * Errors store
     * @type {{PFU0001: string, PFU0002: string}}
     * @private
     */
    private  _errorCodes: PuzzlErrorStore = {
        "PFU0001": "Error.FileUtil.getFilesInDir.FolderNotExists",
        "PFU0002": "Error.FileUtil.getFilesInDir.FileNotExists"
    };

    constructor() {}

    /**
     * Return fileName
     * @param filePath
     * @param withExtension
     * @returns {string}
     */
    public getFileName(filePath: string, withExtension: boolean = false): string {
        let extName = (
                          !withExtension
                      ) ? path.extname(filePath) : null;
        return path.basename(filePath, extName);
    }

    /**
     * Check if directory exist
     * @param path
     * @returns {Promise<boolean>}
     */
    public dirExist(path: string): Promise<boolean> {
        let promiseDirExist = function (resolve: (value: boolean) => void, reject: (error: any) => void): void {
            let callbackStat = function (err: any, stat: Stats): void {
                if (err && err.code !== "ENOENT") {
                    reject(err);
                } else if (stat) {
                    resolve(stat.isDirectory());
                } else {
                    resolve(false);
                }
            };

            fs.stat(path, callbackStat);
        };

        return new Promise(promiseDirExist);
    }

    /**
     * Check if file exist
     * @param path
     * @return {Promise<boolean>}
     */
    public fileExist(path: string): Promise<boolean> {
        let promiseFileExist = function (resolve: (value: boolean) => void, reject: (error: any) => void): void {
            let callbackStatFile = function (err: any, stat: Stats): void {
                if (err && err.code !== "ENOENT") {
                    reject(err);
                } else if (stat) {
                    resolve(stat.isFile());
                } else {
                    resolve(false);
                }
            };

            fs.stat(path, callbackStatFile);
        };

        return new Promise(promiseFileExist);
    }

    /**
     * Read file content at given path
     * @param path
     * @returns {Promise<Buffer|string>}
     */
    public readFile(path: string): Promise<Buffer|string> {
        let resolveFileExist = (exist: boolean): Promise<Buffer|string> => {
            let promiseReadFile = (resolve: (value: Buffer|string) => void, reject: (error: any) => void): void => {
                if (exist) {
                    let callbackReadFile = function (err: any, data: Buffer|string): void {
                        if (err) {
                            reject(err);
                        } else {
                            resolve(data);
                        }
                    };

                    fs.readFile(path, callbackReadFile);
                } else {
                    let err: PuzzlError = this._createError("PFU0002", {path: path});
                    reject(err);
                }
            };

            return new Promise(promiseReadFile);
        };

        return this.fileExist(path).then(resolveFileExist);
    }

    /**
     * Return files
     * @param path
     * @returns {Promise<Array<string>>}
     */
    public getFilesInDir(path: string): Promise<Array<string>> {
        let resolveDirExist = (exist: boolean): Promise<Array<string>> => {
            let promiseReadDir = (resolve: (value: Array<string>) => void, reject: (error: any) => void): void => {
                if (exist) {
                    let callbackReadDir = (err: any, files: Array<string>): void => {
                        if (err) {
                            reject(err);
                        }

                        let fFiles = [];
                        files.forEach(
                            (file: string): void => {
                                let matchPattern = false;
                                this._excludedFilesPatterns.forEach(
                                    function (pattern: string): void {
                                        if ((
                                                new RegExp(pattern)
                                            ).test(file) && !matchPattern) {
                                            matchPattern = true;
                                        }
                                    }
                                );

                                if (!matchPattern) {
                                    fFiles.push(file);
                                }
                            }
                        );
                        resolve(fFiles);
                    };

                    fs.readdir(path, callbackReadDir);
                } else {
                    let err: PuzzlError = this._createError("PFU0001", {path: path});
                    reject(err);
                }
            };

            return new Promise(promiseReadDir);
        };

        return this.dirExist(path)
                   .then(resolveDirExist);
    }

    /**
     * Create an error based on given code
     * @param code
     * @param datas
     * @returns {PuzzlError}
     * @private
     */
    private _createError(code: string, datas?: any): PuzzlError {
        if (code in this._errorCodes) {
            return new PuzzlError(this._errorCodes[code], code, datas);
        }

        return new PuzzlError();
    }
}
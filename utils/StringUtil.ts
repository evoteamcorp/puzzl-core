import {IUtil} from "../interfaces/IUtil";
import {Instantiate} from "../decorators/common/Instantiate";
import {MetaObject} from "../decorators/common/MetaObject";

@Instantiate()
@MetaObject()

/**
 * Static class for managing Strings
 */
export class StringUtil implements IUtil {

    /**
     * Uppercase first letter
     * @param txt
     * @returns {string}
     */
    public ucfirst(txt: string): string {
        txt += "";
        let c = txt.charAt(0).toUpperCase();
        return c + txt.substr(1);
    }

    /**
     * Camelize a string
     * @param txt
     * @returns {string}
     */
    public camelize(txt: string): string {
        txt += "";
        let elements = txt.split("-");

        let finalTxt = "";

        let walkSubElements = (subElement: string) => {
            finalTxt += this.ucfirst(subElement);
        };

        let walkElements = (element: string) => {
            let subElements = element.split("_");

            subElements.forEach(walkSubElements);
        };

        elements.forEach(walkElements);

        return finalTxt;
    }

    /**
     * Uncamelize given txt
     * @param txt
     * @returns {string}
     */
    public uncamelize(txt: string): string {
        txt += "";
        txt = txt.replace(/([A-Z])/g, "_$1");
        txt = txt.toLocaleLowerCase();
        txt = txt.replace(/^_/, "");
        txt = txt.replace(/_+/g, "_");
        return txt;
    }

    /**
     * Clean the txt
     * @param txt
     * @returns {string}
     */
    public clean(txt: string): string {
        return txt.replace(/[^A-Z0-9a-z]/gi, "_");
    }
}
"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var Instantiate_1 = require("../decorators/common/Instantiate");
var MetaObject_1 = require("../decorators/common/MetaObject");
var StringUtil = (function () {
    function StringUtil() {
    }
    StringUtil.prototype.ucfirst = function (txt) {
        txt += "";
        var c = txt.charAt(0).toUpperCase();
        return c + txt.substr(1);
    };
    StringUtil.prototype.camelize = function (txt) {
        var _this = this;
        txt += "";
        var elements = txt.split("-");
        var finalTxt = "";
        var walkSubElements = function (subElement) {
            finalTxt += _this.ucfirst(subElement);
        };
        var walkElements = function (element) {
            var subElements = element.split("_");
            subElements.forEach(walkSubElements);
        };
        elements.forEach(walkElements);
        return finalTxt;
    };
    StringUtil.prototype.uncamelize = function (txt) {
        txt += "";
        txt = txt.replace(/([A-Z])/g, "_$1");
        txt = txt.toLocaleLowerCase();
        txt = txt.replace(/^_/, "");
        txt = txt.replace(/_+/g, "_");
        return txt;
    };
    StringUtil.prototype.clean = function (txt) {
        return txt.replace(/[^A-Z0-9a-z]/gi, "_");
    };
    StringUtil = __decorate([
        Instantiate_1.Instantiate(),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [])
    ], StringUtil);
    return StringUtil;
}());
exports.StringUtil = StringUtil;
//# sourceMappingURL=StringUtil.js.map
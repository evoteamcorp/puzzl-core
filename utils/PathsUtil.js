"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var path = require("path");
var Instantiate_1 = require("../decorators/common/Instantiate");
var MetaObject_1 = require("../decorators/common/MetaObject");
var PathsUtil = (function () {
    function PathsUtil() {
        this._appPath = path.dirname(require.main.filename);
        this._puzzlPath = path.resolve(__dirname + "/../");
        this._piecesPath = {};
    }
    PathsUtil.prototype.getApp = function (extraPath) {
        var p = this._appPath;
        if (extraPath != null) {
            p += "/" + extraPath;
        }
        return p;
    };
    PathsUtil.prototype.getPuzzl = function (extraPath) {
        var p = this._puzzlPath;
        if (extraPath != null) {
            p += "/" + extraPath;
        }
        return p;
    };
    PathsUtil.prototype.getPuzzlUtils = function (name) {
        return this.getPuzzl("utils") +
            ((name) ? "/" + name : "");
    };
    PathsUtil.prototype.getPuzzlCores = function (name) {
        return this.getPuzzl("cores") +
            ((name) ? "/" + name : "");
    };
    PathsUtil.prototype.addPiecePath = function (name, path) {
        this._piecesPath[name] = path;
    };
    PathsUtil.prototype.getPiecePath = function (name, extraPath) {
        var p = this._piecesPath[name];
        if (extraPath != null) {
            p += "/" + extraPath;
        }
        return p;
    };
    PathsUtil = __decorate([
        Instantiate_1.Instantiate(),
        MetaObject_1.MetaObject(), 
        __metadata('design:paramtypes', [])
    ], PathsUtil);
    return PathsUtil;
}());
exports.PathsUtil = PathsUtil;
//# sourceMappingURL=PathsUtil.js.map